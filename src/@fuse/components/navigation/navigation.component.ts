import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    ViewEncapsulation,
} from "@angular/core";
import { merge, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { AuthenticationService } from "app/_services";
import { WebRequestService } from "app/_services/web-request.service";
import { map } from "lodash";
import { FuseNavigation } from "@fuse/types";

@Component({
    selector: "fuse-navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FuseNavigationComponent implements OnInit {
    @Input()
    layout = "vertical";

    navigation: any = [];

    // @Input()
    // navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseNavigationService: FuseNavigationService,
        private authenticationService: AuthenticationService,
        private webReqservices: WebRequestService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    findpermission(obj, allowedrole) {
        for (let i = 0; i < obj.length; i++) {
            if (allowedrole == obj[i]) return true;
        }
    }

    /**
     * On init
     */
    ngOnInit(): void {
        //getting the permissions

        // for (let i = 0; i < this.obj.length; i++) {
        //     if (allowedRoles == this.obj[i]) return true;
        // }

        const currenuser = this.authenticationService.currentUserValue;

        this.webReqservices
            .get("users/roles/permissions/" + currenuser["roles"][0].title)
            .subscribe(
                (res) => {
                    console.log("all permissions");
                    console.log(res);
                    let children = [];
                    let children1 = [];
                    var obj: any = {};
                    var obj1: any = {};
                    //////////////////////////
                    obj = {
                        id: "Dashboards",
                        title: "Dashboards",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "dashboard.generale")) {
                        obj.children.push({
                            id: "General Dashboard",
                            title: "General Dashboard",
                            type: "item",
                            url: "/apps/dashboards/analytics",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "dashboard.project")) {
                        obj.children.push({
                            id: "My Dashboard",
                            title: "My Dashboard",
                            type: "item",
                            url: "/apps/dashboards/project",
                            exactMatch: true,
                        });
                    }
                    if (
                        this.findpermission(res, "dashboard.generale") ||
                        this.findpermission(res, "dashboard.project")
                    ) {
                        children.push(obj);
                    }
                    ///////////////////
                    //////////////////////////
                    obj = {
                        id: "Projects Management",
                        title: "Projects Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "project.views")) {
                        obj.children.push({
                            id: "View Projects",
                            title: "View Projects",
                            type: "item",
                            url: "/apps/project/all",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "project.add")) {
                        obj.children.push({
                            id: "Add Project",
                            title: "Add Project",
                            type: "item",
                            url: "/apps/project",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "project.director")) {
                        obj.children.push({
                            id: "Director's Projects",
                            title: "Director's Projects",
                            type: "item",
                            url: "/apps/project/director",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "project.member")) {
                        obj.children.push({
                            id: "Member's Projects",
                            title: "Member's Projects",
                            type: "item",
                            url: "/apps/project/memeber",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "scrum.template")) {
                        obj.children.push({
                            id: "Scrum Board",
                            title: "Scrum template",
                            type: "item",
                            url: "/apps/scrum/boards",
                            exactMatch: true,
                        });
                    }
                    if (
                        this.findpermission(res, "project.views") ||
                        this.findpermission(res, "scrum.template") ||
                        this.findpermission(res, "project.add") ||
                        this.findpermission(res, "project.director") ||
                        this.findpermission(res, "project.member")
                    ) {
                        children.push(obj);
                    }
                    ///////////////////
                    //////////////////////////
                    obj = {
                        id: "Reports",
                        title: "Reports",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "report.send")) {
                        obj.children.push({
                            id: "Send Report",
                            title: "Send Report",
                            type: "item",
                            url: "/apps/addreport",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "report.views")) {
                        obj.children.push({
                            id: "View Reports",
                            title: "View Reports",
                            type: "item",
                            url: "/apps/reports/reports",
                            exactMatch: true,
                        });
                    }
                    if (
                        this.findpermission(res, "report.views") ||
                        this.findpermission(res, "report.send")
                    ) {
                        children.push(obj);
                    }
                    ///////////////////
                    obj = {
                        id: "Chat",
                        title: "Chat",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "chat.views")) {
                        obj.children.push({
                            id: "Chat",
                            title: "Chat",
                            type: "item",
                            url: "/apps/chat",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "chat.views")) {
                        children.push(obj);
                    }
                    //////////////////////////
                    obj1 = {
                        id: "Role Management",
                        title: "Role Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "role.add")) {
                        obj1.children.push({
                            id: "Add Role",
                            title: "Add Role",
                            type: "item",
                            url: "/apps/addrole",
                            exactMatch: true,
                        });
                    }
                    if (this.findpermission(res, "role.views")) {
                        obj1.children.push({
                            id: "View Roles",
                            title: "View Roles",
                            type: "item",
                            url: "/apps/roles",
                            exactMatch: true,
                        });
                    }
                    if (
                        this.findpermission(res, "role.views") ||
                        this.findpermission(res, "role.add")
                    ) {
                        children1.push(obj1);
                    }
                    ///////////////////

                    ///////////////////
                    obj1 = {
                        id: "Permissions",
                        title: "Permissions",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "permission.views")) {
                        obj1.children.push({
                            id: "Permissions",
                            title: "Permissions",
                            type: "item",
                            url: "/apps/roles/permission",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "permission.views")) {
                        children1.push(obj1);
                    }
                    //////////////////////////

                    ///////////////////
                    obj = {
                        id: "Resource Management",
                        title: "Resource Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "resource.views")) {
                        obj.children.push({
                            id: "View Resource",
                            title: "View Resource",
                            type: "item",
                            url: "/apps/resources",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "resource.views")) {
                        children.push(obj);
                    }
                    //////////////////////////
                    ///////////////////
                    obj1 = {
                        id: "User Management",
                        title: "User Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "user.views")) {
                        obj1.children.push({
                            id: "View Users",
                            title: "View Users",
                            type: "item",
                            url: "/apps/users",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "user.views")) {
                        children1.push(obj1);
                    }
                    //////////////////////////
                    ///////////////////
                    obj = {
                        id: "Logs Management",
                        title: "Logs Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "log.views")) {
                        obj.children.push({
                            id: "Logs",
                            title: "Logs",
                            type: "item",
                            url: "/apps/shadow/logs",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "log.views")) {
                        children.push(obj);
                    }
                    //////////////////////////
                    ///////////////////
                    obj = {
                        id: "Team Management",
                        title: "Team Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "team.views")) {
                        obj.children.push({
                            id: "View Teams",
                            title: "View Teams",
                            type: "item",
                            url: "/apps/team/all",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "team.views")) {
                        children.push(obj);
                    }
                    //////////////////////////

                    ///////////////////
                    obj = {
                        id: "Kpi Management",
                        title: "Kpi Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "kpi.views")) {
                        obj.children.push({
                            id: "View Kpi",
                            title: "View Kpi",
                            type: "item",
                            url: "/apps/kpi",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "kpi.views")) {
                        children.push(obj);
                    }
                    //////////////////////////

                    ///////////////////
                    obj = {
                        id: "Customers Expectations",
                        title: "Customers Expectations",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "customer.views")) {
                        obj.children.push({
                            id: "View Expectations",
                            title: "View Expectations",
                            type: "item",
                            url: "/apps/expectation",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "customer.views")) {
                        children.push(obj);
                    }
                    //////////////////////////
                    ///////////////////
                    obj = {
                        id: "Risk Management",
                        title: "Risk Management",
                        translate: "NAV.DASHBOARDS",
                        type: "collapsable",
                        icon: "dashboard",
                        children: [],
                    };
                    if (this.findpermission(res, "risk.views")) {
                        obj.children.push({
                            id: "View Risks",
                            title: "View Risks",
                            type: "item",
                            url: "/apps/risks",
                            exactMatch: true,
                        });
                    }

                    if (this.findpermission(res, "risk.views")) {
                        children.push(obj);
                    }
                    //////////////////////////
                    console.log("children");
                    console.log(children);
                    const navigation1: FuseNavigation[] = [
                        {
                            id: "applications",
                            title: "My Board",
                            translate: "NAV.APPLICATIONS",
                            type: "group",
                            icon: "apps",
                            children: children,
                        },
                        {
                            id: "applications",
                            title: "Admin Board",
                            translate: "NAV.APPLICATIONS",
                            type: "group",
                            icon: "apps",
                            children: children1,
                        },
                    ];
                    console.log("navigation1");
                    console.log(navigation1);

                    this.navigation = navigation1;

                    // {
                    //     id: "Reports",
                    //     title: "Reports",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Send Report",
                    //             title: "Send Report",
                    //             type: "item",
                    //             url: "/apps/addreport",
                    //             exactMatch: true,
                    //         },
                    //         {
                    //             id: "View Reports",
                    //             title: "View Reports",
                    //             type: "item",
                    //             url: "/apps/reports/reports",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Chat Board",
                    //     title: "Chat Board",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Chat",
                    //             title: "Chat",
                    //             type: "item",
                    //             url: "/apps/chat",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Role management",
                    //     title: "Role management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Add Role",
                    //             title: "Add Role",
                    //             type: "item",
                    //             url: "/apps/addrole",
                    //             exactMatch: true,
                    //         },
                    //         {
                    //             id: "View Roles",
                    //             title: "View Roles",
                    //             type: "item",
                    //             url: "/apps/roles",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Permission management",
                    //     title: "Permission management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Permission",
                    //             title: "Permissions",
                    //             type: "item",
                    //             url: "/apps/roles/permission",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Project management",
                    //     title: "Project management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Add Project",
                    //             title: "Add Project",
                    //             type: "item",
                    //             url: "/apps/project",
                    //             exactMatch: true,
                    //         },
                    //         {
                    //             id: "View Projects",
                    //             title: "View Projects",
                    //             type: "item",
                    //             url: "/apps/project/all",
                    //             exactMatch: true,
                    //         },

                    //         {
                    //             id: "Projects Director",
                    //             title: "Projects Director",
                    //             type: "item",
                    //             url: "/apps/project/director",
                    //             exactMatch: true,
                    //         },
                    //         {
                    //             id: "Projects Memeber",
                    //             title: "Projects Memeber",
                    //             type: "item",
                    //             url: "/apps/project/memeber",
                    //             exactMatch: true,
                    //         },
                    //         {
                    //             id: "scrumboard",
                    //             title: "Scrum Templates",
                    //             translate: "NAV.SCRUMBOARD",
                    //             type: "item",

                    //             url: "/apps/scrum/boards",
                    //         },
                    //     ],
                    // },

                    // {
                    //     id: "Resource management",
                    //     title: "Resource management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "View resource",
                    //             title: "View Rources",
                    //             type: "item",
                    //             url: "/apps/resources",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "User management",
                    //     title: "User management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "View Users",
                    //             title: "View Users",
                    //             type: "item",
                    //             url: "/apps/users",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Shadowing",
                    //     title: "Logs management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Logs",
                    //             title: "Logs",
                    //             type: "item",
                    //             url: "/apps/shadow/logs",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Team management",
                    //     title: "Team management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "View Teams",
                    //             title: "View Teams",
                    //             type: "item",
                    //             url: "/apps/team/all",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },

                    // {
                    //     id: "Kpi Management",
                    //     title: "Kpi Management",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "Kpi",
                    //             title: "Kpi",
                    //             type: "item",
                    //             url: "/apps/kpi",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Customers Expectations",
                    //     title: "Custmors Expectations",
                    //     translate: "NAV.DASHBOARDS",
                    //     type: "collapsable",
                    //     icon: "dashboard",
                    //     children: [
                    //         {
                    //             id: "View Expectations",
                    //             title: "View Expectations",
                    //             type: "item",
                    //             url: "/apps/expectation",
                    //             exactMatch: true,
                    //         },
                    //     ],
                    // },
                    // {
                    //     id: "Logout",
                    //     title: "<<<<<<<<<<Logout>>>>>>>>>>>",
                    //     type: "item",
                    //     url: "/pages/auth/login",
                    // },

                    //////////nope

                    //    , {
                    //         id: "scrumboard",
                    //         title: "Scrumboard",
                    //         translate: "NAV.SCRUMBOARD",
                    //         type: "item",
                    //         icon: "assessment",
                    //         url: "/apps/scrumboard"
                    //     }
                },
                (err) => {
                    console.log(err);
                }
            );

        // Load the navigation either from the input or from the service
        // this.navigation =
        //     this.navigation ||
        //     this._fuseNavigationService.getCurrentNavigation();
        // console.log("this.navigation");

        // console.log(this.navigation);
        // Subscribe to the current navigation changes
        this._fuseNavigationService.onNavigationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                // Load the navigation
                // this.navigation = this._fuseNavigationService.getCurrentNavigation();
                console.log(" this.navigation");
                console.log(this.navigation);
                this.navigation = this._fuseNavigationService.getCurrentNavigation();

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to navigation item
        merge(
            this._fuseNavigationService.onNavigationItemAdded,
            this._fuseNavigationService.onNavigationItemUpdated,
            this._fuseNavigationService.onNavigationItemRemoved
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }
}
