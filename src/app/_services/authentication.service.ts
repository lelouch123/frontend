﻿import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import { User } from "../_models";
import { LocationHubService } from "./location-hub.service";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    private currentPermissinsSubject: BehaviorSubject<any>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private location: LocationHubService
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(
            JSON.parse(localStorage.getItem("currentUser"))
        );
        this.currentPermissinsSubject = new BehaviorSubject<any>(
            JSON.parse(localStorage.getItem("currentpermisssion"))
        );
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }
    public get currentPermissinsValue(): any {
        return this.currentPermissinsSubject.value;
    }
    login(email: string, password: string) {
        var Username = email;
        return this.http
            .post<any>(`http://localhost:4000/users/authenticate`, {
                Username,
                password,
            })
            .pipe(
                map((user) => {
                    // login successful if there's a jwt token in the response
                    if (user && user.token) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        this.http
                            .get(
                                "http://localhost:4000/users/roles/permissions/" +
                                    user["roles"][0].title
                            )
                            .subscribe(
                                (permissions) => {
                                    console.log(permissions);
                                    // tslint:disable-next-line: prefer-for-of

                                    localStorage.setItem(
                                        "currentpermisssion",
                                        JSON.stringify(permissions)
                                    );
                                    this.currentPermissinsSubject.next(
                                        permissions
                                    );
                                },
                                (err) => {
                                    console.log(err);
                                }
                            );
                        localStorage.setItem(
                            "currentUser",
                            JSON.stringify(user)
                        );
                        this.location.reconnect();
                        this.currentUserSubject.next(user);
                        // this.location.connect();
                    }

                    return user;
                })
            );
    }

    signup(email: string, password: string, username: string) {
        return this.http
            .post<any>(`http://localhost:4000/user/signup`, {
                email,
                password,
                username,
            })
            .pipe(
                map((user) => {
                    // login successful if there's a jwt token in the response
                    if (user && user.token) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem(
                            "currentUser",
                            JSON.stringify(user)
                        );
                        this.currentUserSubject.next(user);
                    }

                    return user;
                })
            );
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem("currentUser");
        localStorage.removeItem("currentpermisssion");
        this.currentUserSubject.next(null);
        this.currentPermissinsSubject.next(null);
    }
}
