import { Injectable } from "@angular/core";
import { WebRequestService } from "./web-request.service";

@Injectable({
    providedIn: "root"
})
export class RoleService {
    public roles: any = [
        // { Role_title: "Hydrogen", Description: "H" },
        // { Role_title: "Helium", Description: "He" },
        // { Role_title: "Lithium", Description: "Li" },
        // { Role_title: "Beryllium", Description: "Be" },
        // { Role_title: "Boron", Description: "B" },
        // { Role_title: "Carbon", Description: "C" },
        // { Role_title: "Nitrogen", Description: "N" },
        // { Role_title: "Oxygen", Description: "O" },
        // { Role_title: "Fluorine", Description: "F" },
        // { Role_title: "Neon", Description: "Ne" },
        // { Role_title: "Sodium", Description: "Na" },
        // { Role_title: "Magnesium", Description: "Mg" },
        // { Role_title: "Aluminum", Description: "Al" },
        // { Role_title: "Silicon", Description: "Si" },
        // { Role_title: "Phosphorus", Description: "P" },
        // { Role_title: "Sulfur", Description: "S" },
        // { Role_title: "Chlorine", Description: "Cl" },
        // { Role_title: "Argon", Description: "Ar" },
        // { Role_title: "Potassium", Description: "K" },
        // { Role_title: "Calcium", Description: "Ca" }
    ];
    constructor(private webReqservices: WebRequestService) {}
    creatRole(role: string, description: string) {
        return this.webReqservices.post("users/roles/create", {
            role,
            description
        });
    }
    getroles() {
        return this.webReqservices.get("users/roles");
    }
}
