import { Injectable } from "@angular/core";
import * as signalR from "@aspnet/signalr";
import { Subject, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class LocationHubService {
    private connection: signalR.HubConnection;
    connectionEstablished = new Subject<Boolean>();
    locationCordinates = new Subject<any>();
    contacts = new BehaviorSubject<any>([]);
    messasges = new Subject<any>();
    reports = new Subject<any>();
    reconnect() {
        if (JSON.parse(localStorage.getItem("currentUser")).id) {
            this.connection
                .invoke(
                    "getConnectionId",
                    JSON.parse(localStorage.getItem("currentUser")).id
                )
                .then(function (connectionId) {
                    console.log("connectionId");
                    console.log(connectionId);
                    // Send the connectionId to controller
                });
            console.log("calling getcontacts method");
            this.connection.invoke("getContacts").then(function () {});
        }
    }
    connect() {
        if (!this.connection) {
            this.connection = new signalR.HubConnectionBuilder()
                .withUrl("http://localhost:4000/" + "location")
                .build();

            this.connection
                .start()
                .then(() => {
                    console.log("Hub connection started");
                    this.connectionEstablished.next(true);
                    // console.log("userid");
                    // console.log(
                    //     JSON.parse(localStorage.getItem("currentUser")).id
                    // );
                    if (JSON.parse(localStorage.getItem("currentUser")).id) {
                        this.connection
                            .invoke(
                                "getConnectionId",
                                JSON.parse(localStorage.getItem("currentUser"))
                                    .id
                            )
                            .then(function (connectionId) {
                                console.log("connectionId");
                                console.log(connectionId);
                                // Send the connectionId to controller
                            });
                        console.log("calling getcontacts method");
                        this.connection
                            .invoke("getContacts")
                            .then(function () {});
                    }
                })
                .catch((err) => console.log(err));

            this.connection.on("ReceiveMessage", (msg) => {
                this.locationCordinates.next(msg);
            });
            this.connection.on("ReceiveMessage1", (msg) => {
                console.log("message recived");
                this.messasges.next(msg);
            });
            this.connection.on("ReceiveMessage1", (msg) => {
                console.log("message recived");
                this.messasges.next(msg);
            });
            this.connection.on("ReceiveReport", (msg) => {
                console.log("Report recived");
                this.reports.next(msg);
            });
            this.connection.on("UserDisconnected", (msg) => {
                this.locationCordinates.next(msg);
            });
            this.connection.on("getContacts", (msg) => {
                console.log("getContacts");

                console.log(msg);
                this.contacts.next(msg);
            });
        }
    }

    send(message) {
        this.connection
            .invoke("SendMessageToAll", message)
            .catch(function (err) {
                return console.error(err.toString());
            });
    }

    sendtoone(user, msg) {
        this.connection
            .invoke("sendMessageToOne", user, msg)
            .catch(function (err) {
                return console.error(err.toString());
            });
    }

    sendreporttoone(user, report) {
        this.connection
            .invoke("sendReportToOne", user, report)
            .catch(function (err) {
                return console.error(err.toString());
            });
    }
    disconnect() {
        if (this.connection) {
            this.connection.stop();
            this.connection = null;
        }
    }
    constructor() {}
}
