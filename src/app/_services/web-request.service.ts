import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
    providedIn: "root",
})
export class WebRequestService {
    readonly ROOT_URL;

    constructor(private http: HttpClient) {
        this.ROOT_URL = "http://localhost:4000";
    }

    get(uri: String) {
        return this.http.get(`${this.ROOT_URL}/${uri}`);
    }
    post(uri: String, payload: any) {
        console.log(payload);
        return this.http.post(`${this.ROOT_URL}/${uri}`, payload);
    }
    patch(uri: String, payload: object) {
        return this.http.patch(`${this.ROOT_URL}/${uri}`, payload);
    }
    put(uri: String, payload: object) {
        return this.http.put(`${this.ROOT_URL}/${uri}`, payload);
    }
    delete(uri: String) {
        return this.http.delete(`${this.ROOT_URL}/${uri}`);
    }
}
