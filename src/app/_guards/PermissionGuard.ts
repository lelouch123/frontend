import { Injectable } from "@angular/core";
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild,
} from "@angular/router";

import { AuthenticationService } from "../_services";
import { WebRequestService } from "app/_services/web-request.service";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class PermissionGuard implements CanActivate {
    obj: any = [];
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private webReqservices: WebRequestService
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        const currenuser = this.authenticationService.currentUserValue;
        let currentpermissions = [];
        const allowedRoles = route.data.allowedRoles;
        return this.webReqservices
            .get("users/roles/permissions/" + currenuser["roles"][0].title)
            .pipe(
                map((res) => {
                    this.obj = res;
                    for (let i = 0; i < this.obj.length; i++) {
                        if (allowedRoles == this.obj[i]) return true;
                    }
                    console.log(this.obj);
                    this.router.navigate(["pages/errors/error-500"]);
                    return false;
                }),
                catchError((err) => {
                    return of(false);
                })
            );
    }
}
