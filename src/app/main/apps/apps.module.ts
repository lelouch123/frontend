import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FuseSharedModule } from "@fuse/shared.module";
import { TestComponent } from "./test/test.component";
import { AuthGuard } from "app/_guards";
import { PermissionGuard } from "app/_guards/PermissionGuard";
import { ShadowComponent } from "./shadow/shadow.component";
import { MatButtonModule, MatDialogModule } from "@angular/material";

const routes = [
    {
        path: "dashboards/analytics",
        loadChildren:
            "./dashboards/analytics/analytics.module#AnalyticsDashboardModule",
    },
    {
        path: "dashboards/project",
        loadChildren:
            "./dashboards/project/project.module#ProjectDashboardModule",
    },
    {
        path: "mail",
        loadChildren: "./mail/mail.module#MailModule",
    },
    {
        path: "mail-ngrx",
        loadChildren: "./mail-ngrx/mail.module#MailNgrxModule",
    },
    {
        path: "chat",
        loadChildren: "./chat/chat.module#ChatModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "chat.views",
        },
    },
    {
        path: "calendar",
        loadChildren: "./calendar/calendar.module#CalendarModule",
    },
    {
        path: "e-commerce",
        loadChildren: "./e-commerce/e-commerce.module#EcommerceModule",
    },
    {
        path: "academy",
        loadChildren: "./academy/academy.module#AcademyModule",
    },
    {
        path: "todo1",
        loadChildren: "./todo1/todo.module#TodoModule",
    },

    {
        path: "testt",
        loadChildren: "./testt/testt.module#TesttModule",
    },
    {
        path: "file-manager",
        loadChildren: "./file-manager/file-manager.module#FileManagerModule",
    },
    {
        path: "contacts",
        loadChildren: "./contacts/contacts.module#ContactsModule",
    },
    {
        path: "scrumboard",
        loadChildren: "./scrumboard/scrumboard.module#ScrumboardModule",
    },
    {
        path: "scrum",
        loadChildren: "./scrum/scrumboard/scrumboard.module#ScrumboardModule",
    },
    {
        path: "addrole",
        loadChildren: "./add-role/add-role.module#AddRoleModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "role.add",
        },
    },
    {
        path: "addreport",
        loadChildren: "./add-report/add-role.module#AddRoleModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "report.send",
        },
    },
    {
        path: "roles",
        loadChildren: "./roles/roles.module#RolesModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "role.views",
        },
    },
    {
        path: "users",
        loadChildren: "./users/users.module#UsersModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "user.views",
        },
    },
    {
        path: "team",
        loadChildren: "./team/team.module#TeamModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "team.views",
        },
    },
    {
        path: "shadow",
        loadChildren: "./shadow/shadow.module#ShadowModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "log.views",
        },
    },
    {
        path: "reports",
        loadChildren: "./reports/shadow.module#ShadowModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "report.views",
        },
    },
    {
        path: "action",
        loadChildren: "./director-actions/shadow.module#ShadowModule",
    },
    {
        path: "resources",
        loadChildren: "./resource/resource.module#ResourceModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "resource.views",
        },
    },
    {
        path: "risks",
        loadChildren: "./risk/resource.module#ResourceModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "risk.views",
        },
    },
    {
        path: "kpi",
        loadChildren: "./KPI/resource.module#ResourceModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "kpi.views",
        },
    },
    {
        path: "expectation",
        loadChildren: "./expectation/resource.module#ResourceModule",
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "customer.views",
        },
    },
    {
        path: "project",
        loadChildren: "./project/project.module#ProjectModule",
    },
    // {
    //     path: "uploadimage",
    //     loadChildren: "./uploadimage/uploadimage.module#UploadimageModule"
    // },
    // {
    //     path: "favoritimages",
    //     loadChildren: "./favoritimages/favoritimages.module#FavoritimagesModule"
    // },
    // {
    //     path: "videos",
    //     loadChildren: "./videos/videos.module#VideosModule"
    // }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule,
        MatButtonModule,
        MatDialogModule,
    ],
    declarations: [TestComponent, ShadowComponent],
    entryComponents: [ShadowComponent],
})
export class AppsModule {}
