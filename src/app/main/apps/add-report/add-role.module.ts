import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { AddRoleComponent } from "./add-role.component";

import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";

import { FuseSharedModule } from "@fuse/shared.module";

import { FormsComponent } from "app/main/ui/forms/forms.component";
import { MatSnackBarModule } from "@angular/material";

const routes: Routes = [{ path: "", component: AddRoleComponent }];

@NgModule({
    declarations: [AddRoleComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatSnackBarModule,
        FuseSharedModule,
    ],
})
export class AddRoleModule {}
