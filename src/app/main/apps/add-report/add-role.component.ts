import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { RoleService } from "app/_services/role.service";
import { first } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
import { LocationHubService } from "app/_services/location-hub.service";
@Component({
    selector: "app-add-role",
    templateUrl: "./add-role.component.html",
    styleUrls: ["./add-role.component.scss"],
})
export class AddRoleComponent implements OnInit {
    form: FormGroup;
    error: string;
    users: any = [];
    contacts: any = [];
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private _snackBar: MatSnackBar,
        private webReqservices: WebRequestService,
        private location: LocationHubService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Reactive Form

        this.location.contacts.subscribe(
            (loc) => {
                console.log("signal r");
                this.contacts = loc;
                console.log(this.contacts);
            },
            (err) => console.log(err)
        );

        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            report: ["", Validators.required],
            description: ["", Validators.required],
            type: ["", Validators.required],
            reciverid: ["", Validators.required],
        });
        this.webReqservices.get("users").subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.users = res;

                // console.log(this.projects);
            },
            (err) => {
                console.log(err);
            }
        );
    }

    /**
     * On destroy
     */

    onClick() {
        if (this.form.valid) {
            // alert("You Clicked Me!");
            var role = this.form.getRawValue();
            // { role: role["role"] }
            console.log(JSON.stringify(role));

            var user = this.contacts.find((elm) => elm.id == role.reciverid);

            this.webReqservices
                .post("report/create/", {
                    report: role.report,
                    description: role.description,
                    type: role.type,
                    reciverid: role.reciverid,
                    senderid: JSON.parse(localStorage.getItem("currentUser"))
                        .id,
                })
                .subscribe(
                    (data) => {
                        console.log("data");

                        console.log(data);

                        if (user.status.state == "online") {
                            this.location.sendreporttoone(
                                user.status.connectionid,
                                {
                                    report: role.report,
                                    description: role.description,
                                    type: role.type,
                                    reciverid: role.reciverid,
                                    date: data["date"],
                                    senderid: JSON.parse(
                                        localStorage.getItem("currentUser")
                                    ).id,
                                }
                            );
                        }

                        this.form.reset();

                        this.error = "Report successfully Sended";
                    },
                    (error) => {
                        console.log("error");

                        console.log(error);
                        this.error = error;
                    }
                );
        }
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
