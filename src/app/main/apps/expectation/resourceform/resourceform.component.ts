import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
@Component({
    selector: "app-resourceform",
    templateUrl: "./resourceform.component.html",
    styleUrls: ["./resourceform.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceformComponent implements OnInit {
    form: FormGroup;
    error: string;
    projects: any = [];
    constructor(
        public dialogRef: MatDialogRef<ResourceformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        console.log(this.data);

        this.form = this._formBuilder.group({
            customerexpectation: ["", Validators.required],
            priority: ["", Validators.required],
            date: ["", Validators.required],
            source: ["", Validators.required],
            projected: ["", Validators.required],
        });
        this.webReqservices.get("project/all").subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.projects = res;

                // console.log(this.projects);
            },
            (err) => {
                console.log(err);
            }
        );
        if (this.data["type"] != "create") {
            this.webReqservices.get("Expectation/" + this.data["id"]).subscribe(
                (res) => {
                    console.log("res");
                    console.log(res);
                    this.form = this._formBuilder.group({
                        customerexpectation: [
                            res["customerexpectation"],
                            Validators.required,
                        ],
                        priority: [res["priority"], Validators.required],
                        date: [res["date"], Validators.required],
                        source: [res["source"], Validators.required],
                        projected: [res["projected"], Validators.required],
                    });
                },
                (err) => console.log(err)
            );
        }
    }
    onClick() {
        if (this.data["type"] == "create") {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices
                    .post("expectation/create", {
                        customerexpectation: role.customerexpectation,
                        priority: role.priority,
                        date: role.date,
                        source: role.source,
                        projected: role.projected,
                    })
                    .subscribe(
                        (data) => {
                            console.log("data");

                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "Expectation successfully created";
                            this.dialogRef.close({
                                res: "success",
                                resource: data,
                            });
                        },
                        (error) => {
                            console.log("error");

                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        } else {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices
                    .put("expectation/" + this.data["id"], {
                        customerexpectation: role.customerexpectation,
                        priority: role.priority,
                        date: role.date,
                        source: role.source,
                        projected: role.projected,
                    })
                    .subscribe(
                        (data) => {
                            console.log("data");

                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "Expectation successfully updated";
                            this.dialogRef.close({
                                res: "success",
                                resource: role,
                            });
                        },
                        (error) => {
                            console.log("error");

                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
}
