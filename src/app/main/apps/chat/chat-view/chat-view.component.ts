import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewChildren,
    ViewEncapsulation,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { FusePerfectScrollbarDirective } from "@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";

import { ChatService } from "app/main/apps/chat/chat.service";
import { LocationHubService } from "app/_services/location-hub.service";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "chat-view",
    templateUrl: "./chat-view.component.html",
    styleUrls: ["./chat-view.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ChatViewComponent implements OnInit, OnDestroy, AfterViewInit {
    user: any;
    chat: any;
    dialog: any;
    contact: any;
    replyInput: any;
    selectedChat: any;
    contactID: any;
    contacts: any = [];
    @ViewChild(FusePerfectScrollbarDirective, { static: false })
    directiveScroll: FusePerfectScrollbarDirective;

    @ViewChildren("replyInput")
    replyInputField;

    @ViewChild("replyForm", { static: false })
    replyForm: NgForm;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ChatService} _chatService
     */
    constructor(
        private _chatService: ChatService,
        private location: LocationHubService,
        private _httpClient: HttpClient
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.user = JSON.parse(localStorage.getItem("currentUser"));
        this._chatService.onChatSelected
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((chatData) => {
                if (chatData) {
                    this.selectedChat = chatData;
                    console.log("chatData");
                    console.log(this.selectedChat);
                    this.contactID = chatData.contact;
                    console.log("this.contactID");
                    console.log(this.contactID);
                    this.contact = this.contacts.find(
                        (item) => item.id == this.contactID
                    );
                    this.dialog = chatData.dialog;
                    this.readyToReply();
                }
            });
        this._chatService.messagechanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((chatData) => {
                if (chatData) {
                    this.dialog = chatData.find(
                        (elm) =>
                            elm.iduser1 == this.contact.id ||
                            elm.iduser2 == this.contact.id
                    );

                    this.readyToReply();
                }
            });
        this.location.contacts.subscribe((contacts1) => {
            // this.contacts = contacts1;
            this.contacts = contacts1;
            this.contact = this.contacts.find(
                (item) => item.id == this.contactID
            );
        });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
        this.replyInput = this.replyInputField.first.nativeElement;
        this.readyToReply();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Decide whether to show or not the contact's avatar in the message row
     *
     * @param message
     * @param i
     * @returns {boolean}
     */
    shouldShowContactAvatar(message, i): boolean {
        return (
            message.who === this.contact.id &&
            ((this.dialog[i + 1] &&
                this.dialog[i + 1].who !== this.contact.id) ||
                !this.dialog[i + 1])
        );
    }

    /**
     * Check if the given message is the first message of a group
     *
     * @param message
     * @param i
     * @returns {boolean}
     */
    isFirstMessageOfGroup(message, i): boolean {
        return (
            i === 0 ||
            (this.dialog[i - 1] && this.dialog[i - 1].who !== message.who)
        );
    }

    /**
     * Check if the given message is the last message of a group
     *
     * @param message
     * @param i
     * @returns {boolean}
     */
    isLastMessageOfGroup(message, i): boolean {
        return (
            i === this.dialog.length - 1 ||
            (this.dialog[i + 1] && this.dialog[i + 1].who !== message.who)
        );
    }

    /**
     * Select contact
     */
    selectContact(): void {
        this._chatService.selectContact(this.contact);
    }

    /**
     * Ready to reply
     */
    readyToReply(): void {
        setTimeout(() => {
            this.focusReplyInput();
            this.scrollToBottom();
        });
    }

    /**
     * Focus to the reply input
     */
    focusReplyInput(): void {
        setTimeout(() => {
            this.replyInput.focus();
        });
    }

    /**
     * Scroll to the bottom
     *
     * @param {number} speed
     */
    scrollToBottom(speed?: number): void {
        speed = speed || 400;
        if (this.directiveScroll) {
            this.directiveScroll.update();

            setTimeout(() => {
                this.directiveScroll.scrollToBottom(0, speed);
            });
        }
    }

    /**
     * Reply
     */
    reply(event): void {
        event.preventDefault();

        if (!this.replyForm.form.value.message) {
            return;
        }

        // Message
        const message = {
            who: this.user.id,
            message: this.replyForm.form.value.message,
            time: new Date().toISOString(),
            chatid: this.selectedChat.chatId,
        };

        // Add the message to the chat
        this.dialog.push(message);

        console.log("this.dialog");
        console.log(this.dialog);
        // Reset the reply form
        this.replyForm.reset();

        // Update the server
        console.log(this.contact.status.connectionid);

        this._httpClient
            .post(
                "http://localhost:4000/signalr/insert/" +
                    this.selectedChat.chatId,
                message
            )
            .subscribe((response: any) => {
                console.log("chats");

                console.log(response);
                this.location.sendtoone(
                    this.contact.status.connectionid,
                    message
                );
                this.readyToReply();
            });
        // this._chatService
        //     .updateDialog(this.selectedChat.chatId, this.dialog)
        //     .then((response) => {
        //         this.readyToReply();
        //     });
    }
}
