import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ImagesComponent } from './images.component';


const routes: Routes = [
  { path: '', component: ImagesComponent }
];

@NgModule({
  declarations: [ImagesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ImagesModule { }
