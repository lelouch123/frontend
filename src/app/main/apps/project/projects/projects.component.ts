import { Component, OnInit } from "@angular/core";
import { WebRequestService } from "app/_services/web-request.service";
import { MatSnackBar, MatDialog, MatDialogConfig } from "@angular/material";
import { ShadowComponent } from "../../shadow/shadow.component";

@Component({
    selector: "app-projects",
    templateUrl: "./projects.component.html",
    styleUrls: ["./projects.component.scss"],
})
export class ProjectsComponent implements OnInit {
    projects: any;
    projects_o: any;
    searchKey: string = "";
    constructor(
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        this.webReqservices.get("project/all").subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.projects = res;
                this.projects_o = this.projects;
                // console.log(this.projects);
            },
            (err) => {
                console.log(err);
            }
        );
        /* Shadowing operation */
        this.webReqservices
            .post("shadow/", {
                entity: "Project",
                entityid: "all project",
                type: "read",
                user: JSON.parse(localStorage.getItem("currentUser")).name,
            })
            .subscribe(
                (res) => {
                    console.log("shadowing");
                    console.log(res);
                },
                (err) => console.log(err)
            );
        /* End of shadowing operation */
    }
    shadow(id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "30%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { id };
        dialogConfig.maxHeight = "90vh"; //you can adjust the value as per your view
        let dialogRef = this.dialog.open(ShadowComponent, dialogConfig);
    }
    delete(id) {
        this.webReqservices.delete("project/" + id).subscribe(
            (res) => {
                console.log(res);
                this.projects = this.projects.filter((p) => p.id !== id);
                this._snackBar.open("Deleted successfully", "Delete", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Project",
                        entityid: id,
                        type: "delete",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (err) => {
                console.log(err);
            }
        );
    }
    onSearchClear() {
        this.searchKey = "";
        // this.applyFilter();
        this.projects = this.projects_o;
    }

    applyFilter() {
        // this.projects.filter = this.searchKey.trim().toLowerCase();
        // console.log("searchkey");
        // console.log(this.searchKey);
        this.projects = this.projects_o.filter((post) => {
            if (post.title.includes(this.searchKey)) return true;
        });
        //    console.log(
        //         this.projects_o.filter((post) => {
        //             if (post.title.includes(this.searchKey)) return true;
        //         })
        //     );
    }
}
