import { Component, OnInit } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl,
    FormArray,
} from "@angular/forms";
import { WebRequestService } from "app/_services/web-request.service";
import { ActivatedRoute } from "@angular/router";
import { HttpEventType, HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material";

@Component({
    selector: "app-edit",
    templateUrl: "./edit.component.html",
    styleUrls: ["./edit.component.scss"],
})
export class EditComponent implements OnInit {
    form: FormGroup;
    id: string;
    public progress: number;
    public message: string;
    teams: any = [];
    templateid: string = "";
    project = {
        company: "",
        Title: "",
        Projectoverview: "",
        sections: "",
        Startdate: "",
        Enddate: "",
        Image: "",
        Team: [],
    };

    constructor(
        private _formBuilder: FormBuilder,
        private webReqservices: WebRequestService,
        private route: ActivatedRoute,
        private http: HttpClient,
        private _snackBar: MatSnackBar
    ) {}

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.id = params.id;
            this.webReqservices
                .get("project/" + params.id)
                .subscribe((data) => {
                    console.log(data);
                    this.project.Title = data["title"];
                    this.project.company = data["company"];
                    this.project.Startdate = data["startdate"];
                    this.project.Enddate = data["enddate"];
                    this.project.sections = JSON.parse(data["obj"])["sections"];
                    this.project.Projectoverview = data["projectoverview"];
                    this.project.Image = data["image"];
                    this.project.Team = data["teams"];
                    this.templateid = data["templateId"];
                    this.webReqservices.get("team/all").subscribe(
                        (data) => {
                            this.teams = data;
                            console.log("project");
                            console.log(this.project);
                            this.loadForm(this.project);
                            console.log(this.teams);
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
                });
        });
        // Reactive Form

        // this.form = this._formBuilder.group({
        //     company: [
        //         {
        //             value: "Google",
        //             disabled: true,
        //         },
        //         Validators.required,
        //     ],
        //     firstName: ["", Validators.required],
        //     lastName: ["", Validators.required],
        //     address: ["", Validators.required],
        //     address2: ["", Validators.required],
        //     city: ["", Validators.required],
        //     state: ["", Validators.required],
        //     postalCode: ["", [Validators.required, Validators.maxLength(5)]],
        //     country: ["", Validators.required],
        // });

        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            Title: new FormControl(""),
            Projectoverview: new FormControl(""),
            Startdate: new FormControl(""),
            Enddate: new FormControl(""),
            Image: new FormControl(""),
            sections: new FormArray([]),
            Team: [],
        });
    }

    patchForm() {
        this.form.patchValue({
            company: this.project.company,
            Title: this.project.Title,
            Projectoverview: this.project.Projectoverview,
            sections: this.project.sections,
            Startdate: this.project.Startdate,
            Enddate: this.project.Enddate,
            Image: this.project.Image,
            Team: this.project.Team,
        });
    }
    loadForm(data) {
        // create lines array first

        for (let line = 0; line < data.sections.length; line++) {
            const linesFormArray = this.form.get("sections") as FormArray;
            linesFormArray.push(this.initSection());
            console.log("data.sections[line]");

            for (
                let player = 0;
                player < data.sections[line].Fields.length;
                player++
            ) {
                const playersFormsArray = linesFormArray
                    .at(line)
                    .get("Fields") as FormArray;
                playersFormsArray.push(this.initField());
            }
        }

        this.patchForm();
    }
    initField() {
        return new FormGroup({
            Title: new FormControl(""),
            Type: new FormControl(""),
            Value: new FormControl(""),
        });
    }
    initSection() {
        return new FormGroup({
            sectionTitle: new FormControl(""),
            sectionDescription: new FormControl(""),
            Fields: new FormArray([
                // this.initField()
            ]),
        });
    }
    addSection() {
        const control = <FormArray>this.form.get("sections");
        control.push(this.initSection());
    }

    removeSection(i) {
        const control = <FormArray>this.form.get("sections");
        control.removeAt(i);
    }

    getSections(form) {
        //console.log(form.get('sections').controls);
        return form.controls.sections.controls;
    }

    addField(j, title, type) {
        // console.log("title");
        // console.log(title);
        // console.log("type");
        // console.log(type);
        // console.log("j =" + j);
        const control = <FormArray>(
            this.form.get("sections")["controls"][j].get("Fields")
        );
        console.log("control");

        control.push(
            new FormGroup({
                Title: new FormControl(title),
                Type: new FormControl(type),
                Value: new FormControl(""),
            })
        );
    }

    getField(form) {
        //console.log(form.controls.questions.controls);
        return form.controls.Fields.controls;
    }

    removeField(j, i) {
        const control = <FormArray>(
            this.form.get("sections")["controls"][i].get("Fields")
        );
        control.removeAt(j);
    }
    Send() {
        //  let j={Title:'',
        //  description:'',
        //  Tab:''};
        // j =JSON.stringify(this.survey.value);
        //  console.log(j);
        let v = {
            company: "",
            Title: "",
            Projectoverview: "",
            sections: {},
            Startdate: "",
            Enddate: "",
            Image: "",
            Teams: [],
            templateId: this.templateid,
        };
        v.Title = this.form.value.Title;
        v.company = v.company = "Focus";
        v.Projectoverview = this.form.value.Projectoverview;
        v.Startdate = this.form.value.Startdate;
        v.Enddate = this.form.value.Enddate;
        v.Image = this.form.value.Image;
        v.Teams = this.form.value.Team;
        v.sections = { sections: this.form.value.sections };

        console.log(v);
        // this.httpservices.post("api/projectoverview", v).subscribe();
        if (this.form.valid) {
            this.webReqservices.put("project/" + this.id, v).subscribe(
                (res) => {
                    console.log(res);
                    this._snackBar.open("Saved successfully", "save", {
                        duration: 2000,
                    });
                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "project",
                            entityid: this.id,
                            type: "edit",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                },
                (err) => console.log(err)
            );
        }
    }

    public uploadFile = (files) => {
        if (files.length === 0) {
            return;
        }
        console.log("files[0]");
        console.log(files[0]);
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append("file", fileToUpload, fileToUpload.name);

        this.http
            .post("http://localhost:4000/project/images", formData, {
                reportProgress: true,
                observe: "events",
            })
            .subscribe((event) => {
                if (event.type === HttpEventType.UploadProgress)
                    this.progress = Math.round(
                        (100 * event.loaded) / event.total
                    );
                else if (event.type === HttpEventType.Response) {
                    this.message = "Upload success.";
                    console.log(event.body["dbPath"]);
                    this._snackBar.open("Uploaded successfully", "Upload", {
                        duration: 2000,
                    });
                    this.form.patchValue({
                        Image: event.body["dbPath"],
                    });
                }
            });

        // this.webReqservices.post("project/images", formData).subscribe(
        //     (res) => console.log(res),

        //     (err) => console.log(err)
        // );
    };
}
