import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { fuseAnimations } from "@fuse/animations";
import { ProfileService } from "app/main/pages/profile/profile.service";
import { WebRequestService } from "app/_services/web-request.service";
import { ActivatedRoute } from "@angular/router";
import * as Chart from "chart.js";

@Component({
    selector: "profile-about",
    templateUrl: "./about.component.html",
    styleUrls: ["./about.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class ProfileAboutComponent implements OnInit {
    about: any;
    i: number = 0;
    Members: any = [];
    Users: any = [];
    Resources: any = [];
    Expectations: any = [];
    Kpis: any = [];
    public barChartOptions = {
        scaleShowValues: true,

        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        autoSkip: false,
                    },
                },
            ],
        },
    };
    public barChartLabels = [""];
    public barChartType = "bar";
    public barChartLegend = true;
    public option = {
        scaleShowValues: true,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        autoSkip: false,
                    },
                },
            ],
        },
    };
    public barChartData = [
        { data: [65], label: "Target" },
        { data: [28], label: "Kpi" },
    ];
    public datasets: any = [];
    LineChart: any = [];
    project = {
        company: "",
        Title: "",
        Projectoverview: "",
        sections: "",
        Startdate: "",
        Enddate: "",
        Image: "",
        Teams: [],
        resources: [],
    };

    // Private
    // private _unsubscribeAll: Subject<any>;

    constructor(
        private webReqservices: WebRequestService,
        private route: ActivatedRoute
    ) {
        // Set the private defaults
        // this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    barchart() {
        return (this.barChartData = [
            { data: [60], label: "Target" },
            { data: [28], label: "Kpi" },
        ]);
    }
    /**
     * On init
     */
    ngOnInit(): void {
        // this._profileService.aboutOnChanged
        //     .pipe(takeUntil(this._unsubscribeAll))
        //     .subscribe(about => {
        //         this.about = about;
        //     });
        // Line chart:
        this.LineChart = new Chart("lineChart1", {
            type: "line",
            data: {
                labels: [
                    "Jan",
                    "Feb",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec",
                ],
                datasets: [
                    {
                        label: "Number of Items Sold in Months",
                        data: [9, 7, 3, 5, 2, 10, 15, 16, 19, 3, 1, 9],
                        fill: false,
                        lineTension: 0.2,
                        borderColor: "red",
                        borderWidth: 1,
                    },
                ],
            },
            options: {
                title: {
                    text: "Line Chart",
                    display: true,
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                    ],
                },
            },
        });

        this.route.params.subscribe((params) => {
            this.webReqservices.get("resource/all").subscribe(
                (resources) => {
                    console.log("resource");
                    this.Resources = resources;
                    console.log(this.Resources);

                    this.webReqservices
                        .get("project/" + params.id)
                        .subscribe((data) => {
                            /* Shadowing operation */
                            this.webReqservices
                                .post("shadow/", {
                                    entity: "project",
                                    entityid: data["id"],
                                    type: "read",
                                    user: JSON.parse(
                                        localStorage.getItem("currentUser")
                                    ).name,
                                })
                                .subscribe(
                                    (res) => {
                                        console.log("shadowing");
                                        console.log(res);
                                    },
                                    (err) => console.log(err)
                                );
                            /* End of shadowing operation */
                            console.log(data);
                            var id = data["id"];
                            this.project.Title = data["title"];
                            this.project.company = data["company"];
                            this.project.Startdate = data["startdate"];
                            this.project.Enddate = data["enddate"];
                            this.project.sections = JSON.parse(data["obj"])[
                                "sections"
                            ];
                            this.project.Projectoverview =
                                data["projectoverview"];
                            this.project.Image = data["image"];
                            this.project.Teams = data["teams"];
                            this.project.resources = data["resources"];
                            // this.project.Teams.forEach(element => {

                            // });
                            this.webReqservices
                                .get("kpi/detaill/project/" + id)
                                .subscribe(
                                    (res) => {
                                        this.Kpis = res;
                                        this.Kpis.forEach((element) => {
                                            element.barChartData = [
                                                {
                                                    data: [element.target],
                                                    label: "Target",
                                                },
                                                {
                                                    data: [element.kpivalue],
                                                    label: "Kpi",
                                                },
                                            ];
                                            element.barChartLabels = [
                                                element.name,
                                            ];
                                        });
                                        console.log("this.Kpis");
                                        console.log(this.Kpis);
                                    },
                                    (err) => {
                                        console.log(err);
                                    }
                                );

                            this.webReqservices
                                .post("team/members", this.project.Teams)
                                .subscribe(
                                    (data) => {
                                        console.log("members");
                                        this.Members = data;
                                        console.log(this.Members);

                                        this.webReqservices
                                            .get("users")
                                            .subscribe(
                                                (data) => {
                                                    // this.router.navigate(["/apps/images"]);

                                                    this.Users = data;
                                                    this.Users = this.Users.filter(
                                                        (elmm) => {
                                                            return this.Members.some(
                                                                (elm) =>
                                                                    elm ==
                                                                    elmm.id
                                                            );
                                                        }
                                                    );

                                                    console.log("Users");
                                                    console.log(this.Users);
                                                },
                                                (error) => {
                                                    console.log(error);
                                                }
                                            );
                                    },
                                    (erreur) => {
                                        console.log(erreur);
                                    }
                                );
                            //Expectations
                            this.webReqservices
                                .get("expectation/allforproject/" + id)
                                .subscribe(
                                    (data) => {
                                        // this.router.navigate(["/apps/images"]);

                                        this.Expectations = data;

                                        console.log("Expectations");
                                        console.log(this.Expectations);
                                    },
                                    (error) => {
                                        console.log(error);
                                    }
                                );
                            console.log("project");
                            console.log(this.project);
                        });
                },
                (erreur) => {
                    console.log(erreur);
                }
            );
        });
    }

    getresourcename(id) {
        // console.log("debug");

        for (let i = 0; i < this.Resources.length; i++) {
            if (this.Resources[i].id == id) {
                // console.log(this.Resources[i].name);
                return this.Resources[i].name;
            }
        }
    }

    /**
     * On destroy
     */
    // ngOnDestroy(): void
    // {
    //     // Unsubscribe from all subscriptions
    //     this._unsubscribeAll.next();
    //     this._unsubscribeAll.complete();
    // }
}
