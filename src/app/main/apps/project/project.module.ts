import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { ProjectComponent } from "./project.component";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatChipsModule } from "@angular/material/chips";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { EditComponent } from "./edit/edit.component";
import { ProjectsComponent } from "./projects/projects.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { FormsModule } from "@angular/forms";
import { DetailComponent } from "./detail/detail.component";
import { ProfileAboutComponent } from "./about/about.component";
import { MatDividerModule } from "@angular/material/divider";

import { MatTabsModule } from "@angular/material/tabs";
import { AuthGuard } from "app/_guards";
import { ShadowComponent } from "../shadow/shadow.component";
import { MatDialogModule } from "@angular/material";
import { ProjectsdComponent } from "./projects-directors/projects.component";
import { ProjectsmComponent } from "./projects-members/projects.component";
import { ChartsModule } from "ng2-charts";
import { PermissionGuard } from "app/_guards/PermissionGuard";
const routes: Routes = [
    {
        path: "",
        component: ProjectComponent,
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "project.add",
        },
    },
    {
        path: "all",
        component: ProjectsComponent,
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "project.views",
        },
    },
    {
        path: "director",
        component: ProjectsdComponent,
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "project.director",
        },
    },
    {
        path: "memeber",
        component: ProjectsmComponent,
        canActivate: [PermissionGuard],
        data: {
            allowedRoles: "project.member",
        },
    },
    {
        path: "edit/:id",
        component: EditComponent,
    },
    { path: "detaill/:id", component: DetailComponent },
    { path: "about/:id", component: ProfileAboutComponent },
];

@NgModule({
    declarations: [
        ProjectComponent,
        EditComponent,
        ProjectsComponent,
        DetailComponent,
        ProfileAboutComponent,
        ProjectsdComponent,
        ProjectsmComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseSharedModule,
        MatChipsModule,
        MatDatepickerModule,
        MatSnackBarModule,
        FormsModule,
        MatDividerModule,
        FuseSharedModule,
        MatDialogModule,
        ChartsModule,
    ],
    entryComponents: [],
})
export class ProjectModule {}
