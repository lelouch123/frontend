import { Component, OnInit } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl,
    FormArray,
} from "@angular/forms";
import { WebRequestService } from "app/_services/web-request.service";
import { HttpEventType, HttpClient } from "@angular/common/http";
import { MatSnackBar, MatDialog } from "@angular/material";
@Component({
    selector: "app-project",
    templateUrl: "./project.component.html",
    styleUrls: ["./project.component.css"],
})
export class ProjectComponent implements OnInit {
    form: FormGroup;
    public progress: number;
    public message: string;
    teams: any = [];
    resoruces: any = [];
    templates: any = [];
    users: any = [];
    constructor(
        private _formBuilder: FormBuilder,
        private webReqservices: WebRequestService,
        private http: HttpClient,
        private _snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        // Reactive Form

        // this.form = this._formBuilder.group({
        //     company: [
        //         {
        //             value: "Google",
        //             disabled: true,
        //         },
        //         Validators.required,
        //     ],
        //     firstName: ["", Validators.required],
        //     lastName: ["", Validators.required],
        //     address: ["", Validators.required],
        //     address2: ["", Validators.required],
        //     city: ["", Validators.required],
        //     state: ["", Validators.required],
        //     postalCode: ["", [Validators.required, Validators.maxLength(5)]],
        //     country: ["", Validators.required],
        // });Resource
        this.webReqservices.get("team/all").subscribe(
            (data) => {
                this.teams = data;

                console.log(this.teams);
            },
            (error) => {
                console.log(error);
            }
        );
        this.webReqservices.get("project/Board/templates").subscribe(
            (data) => {
                this.templates = data["boards"];

                console.log(this.templates);
            },
            (error) => {
                console.log(error);
            }
        );
        this.webReqservices.get("resource/all").subscribe(
            (data) => {
                this.resoruces = data;

                console.log(this.resoruces);
            },
            (error) => {
                console.log(error);
            }
        );
        this.webReqservices.get("users").subscribe(
            (data) => {
                this.users = data;
                console.log(data);
            },
            (err) => {
                console.log(err);
            }
        );
        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            Title: new FormControl(""),
            Projectoverview: new FormControl(""),
            Startdate: new FormControl(""),
            Enddate: new FormControl(""),
            Image: new FormControl(""),
            Team: new FormControl(""),
            sections: new FormArray([]),
            Resources: new FormArray([]),
            template: new FormControl(""),
            responsable: new FormControl(""),
        });
    }

    initResources() {
        return new FormGroup({
            Name: new FormControl(""),
            Number: new FormControl("", [
                Validators.required,
                Validators.pattern("^[0-9]*$"),
                Validators.minLength(1),
            ]),
            Startdate: new FormControl(""),
            Enddate: new FormControl(""),
        });
    }
    addResources() {
        const control = <FormArray>this.form.get("Resources");
        control.push(this.initResources());
    }
    removeResources(i) {
        const control = <FormArray>this.form.get("Resources");
        control.removeAt(i);
    }

    getResources(form) {
        //console.log(form.get('sections').controls);
        return form.controls.Resources.controls;
    }
    /////
    initSection() {
        return new FormGroup({
            sectionTitle: new FormControl(""),
            sectionDescription: new FormControl(""),
            Fields: new FormArray([
                // this.initField()
            ]),
        });
    }
    addSection() {
        const control = <FormArray>this.form.get("sections");
        control.push(this.initSection());
    }

    removeSection(i) {
        const control = <FormArray>this.form.get("sections");
        control.removeAt(i);
    }

    getSections(form) {
        //console.log(form.get('sections').controls);
        return form.controls.sections.controls;
    }

    addField(j, title, type) {
        // console.log("title");
        // console.log(title);
        // console.log("type");
        // console.log(type);
        // console.log("j =" + j);
        const control = <FormArray>(
            this.form.get("sections")["controls"][j].get("Fields")
        );
        console.log("control");

        control.push(
            new FormGroup({
                Title: new FormControl(title),
                Type: new FormControl(type),
                Value: new FormControl(""),
            })
        );
    }

    getField(form) {
        //console.log(form.controls.questions.controls);
        return form.controls.Fields.controls;
    }

    removeField(j, i) {
        const control = <FormArray>(
            this.form.get("sections")["controls"][i].get("Fields")
        );
        control.removeAt(j);
    }
    Send() {
        //  let j={Title:'',
        //  description:'',
        //  Tab:''};
        // j =JSON.stringify(this.survey.value);
        //  console.log(j);
        let v = {
            company: "",
            Title: "",
            Image: "",
            Projectoverview: "",
            sections: {},
            Startdate: "",
            Enddate: "",
            Teams: [],
            resources: [],
            template: [],
            responsable: "",
        };
        v.Title = this.form.value.Title;
        v.company = "Focus";
        v.Projectoverview = this.form.value.Projectoverview;
        v.Startdate = this.form.value.Startdate;
        v.Enddate = this.form.value.Enddate;
        v.Image = this.form.value.Image;
        v.sections = { sections: this.form.value.sections };
        console.log("v.sections");
        console.log(v.sections);
        v.Teams = this.form.value.Team;
        v.template = this.form.value.template;
        v.resources = this.form.value.Resources;
        v.responsable = this.form.value.responsable;
        console.log(this.form.value);

        console.log(v.Teams);
        // this.httpservices.post("api/projectoverview", v).subscribe();
        /////////////

        if (this.form.valid) {
            console.log("valide");
            this.webReqservices.post("project/create", v).subscribe(
                (res) => {
                    console.log(res);
                    this._snackBar.open("Saved successfully", "Save", {
                        duration: 2000,
                    });
                    this.form = this._formBuilder.group({
                        company: [
                            {
                                value: "Focus",
                                disabled: true,
                            },
                            Validators.required,
                        ],
                        Title: new FormControl(""),
                        Projectoverview: new FormControl(""),
                        Startdate: new FormControl(""),
                        Enddate: new FormControl(""),
                        Image: new FormControl(""),
                        Teams: new FormControl(""),
                        sections: new FormArray([]),
                        Resources: new FormArray([]),
                        template: new FormControl(""),
                        responsable: new FormControl(""),
                    });

                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "project",
                            entityid: res["id"],
                            type: "create",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                },
                (err) => console.log(err)
            );
        }
    }

    public uploadFile = (files) => {
        if (files.length === 0) {
            return;
        }
        console.log("files[0]");
        console.log(files[0]);
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append("file", fileToUpload, fileToUpload.name);

        this.http
            .post("http://localhost:4000/project/images", formData, {
                reportProgress: true,
                observe: "events",
            })
            .subscribe((event) => {
                if (event.type === HttpEventType.UploadProgress)
                    this.progress = Math.round(
                        (100 * event.loaded) / event.total
                    );
                else if (event.type === HttpEventType.Response) {
                    this.message = "Upload success.";
                    console.log(event.body["dbPath"]);
                    this._snackBar.open("Uploaded successfully", "Upload", {
                        duration: 2000,
                    });
                    this.form.patchValue({
                        Image: event.body["dbPath"],
                    });
                }
            });

        // this.webReqservices.post("project/images", formData).subscribe(
        //     (res) => console.log(res),

        //     (err) => console.log(err)
        // );
    };
}
