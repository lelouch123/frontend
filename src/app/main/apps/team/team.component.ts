import { Component, OnInit } from "@angular/core";
import { LocationHubService } from "app/_services/location-hub.service";

@Component({
    selector: "app-team",
    templateUrl: "./team.component.html",
    styleUrls: ["./team.component.css"],
})
export class TeamComponent implements OnInit {
    list: any = ["aaa", "bbb", "ccc", "dddd"];
    selectedValue: null;

    constructor(private location: LocationHubService) {}

    ngOnInit() {
        this.location.connect();
        this.location.locationCordinates.subscribe((loc) => {
            console.log("signal r");
            console.log(loc);
        });
        this.location.contacts.subscribe((contacts) => {
            console.log("contacts");
            console.log(contacts);
        });
    }

    affiche() {
        console.log(this.selectedValue);
    }
    send() {
        this.location.send("hey there");
    }
    sendtoone(id) {
        console.log(id);
        // this.location.sendtoone(id);
    }
}
