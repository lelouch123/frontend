import {
    Component,
    ElementRef,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";

import { fuseAnimations } from "@fuse/animations";

import {
    MatTableDataSource,
    MatDialog,
    MatDialogConfig,
    MatSnackBar,
} from "@angular/material";
import { RoleformComponent } from "../roleform/roleform.component";
import { WebRequestService } from "app/_services/web-request.service";
import { AddmemberComponent } from "../addmember/addmember.component";
import { ShadowComponent } from "../../shadow/shadow.component";

@Component({
    selector: "e-commerce-orders",
    templateUrl: "./orders.component.html",
    styleUrls: ["./orders.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class ExpertiseComponent implements OnInit {
    dataSource: MatTableDataSource<any>;
    displayedColumns = ["maitrise", "note", "weight", "Actions"];
    searchKey: string;
    List: any;
    Listo: any;

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild("filter", { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;
    constructor(
        private dialog: MatDialog,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar
    ) {}
    ngOnInit(): void {
        this.webReqservices.get("team/all").subscribe(
            (data) => {
                this.List = data;
                this.Listo = data;
                console.log(this.List);
                this.dataSource = new MatTableDataSource<any>(this.List);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "team",
                        entityid: "all team",
                        type: "read",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (error) => {
                console.log(error);
            }
        );
    }
    add() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";

        dialogConfig.data = { id: "dqdqqddq", type: "create" };
        let dialogRef = this.dialog.open(AddmemberComponent, dialogConfig);
    }
    create() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";

        dialogConfig.data = { id: "dqdqqddq", type: "create" };
        let dialogRef = this.dialog.open(RoleformComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                this.List.push(result.team);
                console.log(this.List);
                this.dataSource = new MatTableDataSource<any>(this.List);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                this._snackBar.open("Saved successfully", "Save", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "team",
                        entityid: this.List.id,
                        type: "create",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }
    edit(id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";

        dialogConfig.data = { id: id, type: "edit" };
        let dialogRef = this.dialog.open(RoleformComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                this.List = this.List.filter((value, key) => {
                    if (value.id == id) {
                        value.name = result.team.name;
                        value.departement = result.team.departement;
                        value.description = result.team.description;
                        value.image = result.team.image;
                    }
                    return true;
                });
                console.log(this.List);
                // this.dataSource = new MatTableDataSource<any>(this.List);
                // setTimeout(() => (this.dataSource.paginator = this.paginator));
                // setTimeout(() => (this.dataSource.sort = this.sort));
                this._snackBar.open("Saved successfully", "Save", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "team",
                        entityid: id,
                        type: "update",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }
    delete(id) {
        this.webReqservices.delete("team/delete/" + id).subscribe(
            (data) => {
                console.log(data);
                this.List = this.List.filter((obj) => obj.id != id);
                this.Listo = this.Listo.filter((obj) => obj.id != id);

                console.log(this.List);
                this.dataSource = new MatTableDataSource<any>(this.List);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "team",
                        entityid: id,
                        type: "delete",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (error) => {
                console.log(error);
            }
        );
    }
    shadow(id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "30%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { id };
        dialogConfig.maxHeight = "90vh"; //you can adjust the value as per your view
        let dialogRef = this.dialog.open(ShadowComponent, dialogConfig);
    }
    search() {
        this.List = this.Listo.filter((post) => {
            let name = post.name.toLowerCase();
            if (name.includes(this.searchKey.toLowerCase())) return true;
        });
        this.dataSource = new MatTableDataSource<any>(this.List);
        setTimeout(() => (this.dataSource.paginator = this.paginator));
        setTimeout(() => (this.dataSource.sort = this.sort));
    }
}
