import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
import { HttpEventType, HttpClient } from "@angular/common/http";

@Component({
    selector: "app-roleform",
    templateUrl: "./roleform.component.html",
    styleUrls: ["./roleform.component.scss"],
})
export class RoleformComponent implements OnInit {
    form: FormGroup;
    error: string;

    constructor(
        public dialogRef: MatDialogRef<RoleformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        private _snackBar: MatSnackBar,
        private http: HttpClient,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        console.log(this.data);
        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            Name: ["", Validators.required],
            Departement: ["", Validators.required],
            Description: ["", Validators.required],
            Image: new FormControl(""),
        });
        if (this.data["type"] == "edit") {
            this.webReqservices
                .get("team/detaill/" + this.data["id"])
                .subscribe(
                    (res) => {
                        console.log("res");
                        console.log(res);
                        this.form = this._formBuilder.group({
                            company: [
                                {
                                    value: "Focus",
                                    disabled: true,
                                },
                                Validators.required,
                            ],
                            Name: [res["name"], Validators.required],
                            Departement: [
                                res["departement"],
                                Validators.required,
                            ],
                            Description: [
                                res["description"],
                                Validators.required,
                            ],
                            Image: new FormControl(res["image"]),
                        });
                    },
                    (err) => console.log(err)
                );
        }
    }
    onClick() {
        if (this.form.valid && this.data["type"] == "create") {
            // alert("You Clicked Me!");
            var team = this.form.getRawValue();
            // { role: role["role"] }
            console.log(JSON.stringify(team));

            this.webReqservices
                .post("team", {
                    Name: team.Name,
                    Description: team.Description,
                    Image: team.Image,
                    Departement: team.Departement,
                })
                .subscribe(
                    (data) => {
                        console.log(data);
                        // this.router.navigate(["/apps/images"]);
                        this.form.reset();
                        this.error = "Role successfully created";

                        this.dialogRef.close({
                            res: "success",
                            team: data,
                        });
                    },
                    (error) => {
                        console.log(error);
                        this.error = error;
                        // this.loading = false;
                        this.dialogRef.close({ res: "err" });
                    }
                );
        }
        if (this.form.valid && this.data["type"] == "edit") {
            // alert("You Clicked Me!");
            var team = this.form.getRawValue();
            // { role: role["role"] }
            console.log(JSON.stringify(team));

            this.webReqservices
                .put("team/edit/" + this.data["id"], {
                    Name: team.Name,
                    Description: team.Description,
                    Image: team.Image,
                    Departement: team.Departement,
                })
                .subscribe(
                    (data) => {
                        console.log(data);
                        // this.router.navigate(["/apps/images"]);
                        this.form.reset();
                        this.error = "Team successfully created";

                        this.dialogRef.close({
                            res: "success",
                            team: data,
                        });
                    },
                    (error) => {
                        console.log(error);
                        this.error = error;
                        // this.loading = false;
                        this.dialogRef.close({ res: "err" });
                    }
                );
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
    public uploadFile = (files) => {
        if (files.length === 0) {
            return;
        }
        console.log("files[0]");
        console.log(files[0]);
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append("file", fileToUpload, fileToUpload.name);

        this.http
            .post("http://localhost:4000/project/images", formData, {
                reportProgress: true,
                observe: "events",
            })
            .subscribe((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    // this.progress = Math.round(
                    //     (100 * event.loaded) / event.total
                    // );
                } else if (event.type === HttpEventType.Response) {
                    // this.message = "Upload success.";
                    console.log(event.body["dbPath"]);
                    this._snackBar.open("Uploaded successfully", "Upload", {
                        duration: 2000,
                    });
                    this.form.patchValue({
                        Image: event.body["dbPath"],
                    });
                }
            });

        // this.webReqservices.post("project/images", formData).subscribe(
        //     (res) => console.log(res),

        //     (err) => console.log(err)
        // );
    };
}
