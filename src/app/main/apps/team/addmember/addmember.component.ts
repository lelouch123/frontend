import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    ViewChild,
    Inject,
} from "@angular/core";
import {
    MatTableDataSource,
    MatPaginator,
    MatSort,
    MatDialog,
    MatSnackBar,
    MAT_DIALOG_DATA,
    MatDialogRef,
} from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";

@Component({
    selector: "app-addmember",
    templateUrl: "./addmember.component.html",
    styleUrls: ["./addmember.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddmemberComponent implements OnInit {
    displayedColumns: string[] = [
        "roles",
        "image",
        "name",
        "lastname",
        "age",

        "actions",
    ];
    dataSource: MatTableDataSource<any>;
    resources: any = [];
    membersToAdd: any = [];
    // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    searchKey: string = "";
    constructor(
        private dialog: MatDialog,
        public dialogRef: MatDialogRef<AddmemberComponent>,

        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        this.webReqservices.get("users").subscribe(
            (data) => {
                // this.router.navigate(["/apps/images"]);
                this.resources = data;
                console.log(this.resources);
                let members = [];
                if (this.data["members"]) members = this.data["members"];
                this.resources = this.resources.filter((elmm) => {
                    return !members.some((elm) => elm == elmm.id);
                });
                this.dataSource = new MatTableDataSource<any>(this.resources);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
            },
            (error) => {
                console.log(error);
            }
        );

        // this.dataSource.sort = this.sort;
    }

    addmember(id) {
        console.log("id");
        console.log(id);
        this.membersToAdd.push(id);
        console.log("this.membersToAdd");
        console.log(this.membersToAdd);
        this.resources = this.resources.filter((elmm) => {
            return elmm.id != id;
        });
        console.log("resources");
        console.log(this.resources);
        this.dataSource = new MatTableDataSource<any>(this.resources);
        setTimeout(() => (this.dataSource.paginator = this.paginator));
        setTimeout(() => (this.dataSource.sort = this.sort));
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
    onClick() {
        if (this.membersToAdd != []) {
            this.webReqservices
                .put("team/editmembers/" + this.data["id"], {
                    members: this.membersToAdd,
                })
                .subscribe(
                    (data) => {
                        console.log(data);
                        // this.router.navigate(["/apps/images"]);

                        // this.dialogRef.close({
                        //     res: "success",
                        //     name: role.name,
                        //     description: role.description,
                        //     number: role.number,
                        // });
                        this.dialogRef.close({
                            res: "success",
                            members: this.membersToAdd,
                        });
                    },
                    (error) => {
                        console.log(error);

                        // this.loading = false;
                        this.dialogRef.close({ res: "err" });
                    }
                );
        } else {
            this.dialogRef.close({ res: "cancel" });
        }
    }
}
