import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { WebRequestService } from "app/_services/web-request.service";
import { MatSnackBar, MatDialogConfig, MatDialog } from "@angular/material";
import { AddmemberComponent } from "../addmember/addmember.component";
import { isThisSecond } from "date-fns";

@Component({
    selector: "app-member",
    templateUrl: "./member.component.html",
    styleUrls: ["./member.component.scss"],
})
export class MemberComponent implements OnInit {
    searchKey: string = "";
    Members: any = [];
    id: string = "";
    Users: any;
    Userso: any;
    constructor(
        private route: ActivatedRoute,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        this.route.params.subscribe((params) => {
            console.log(params.id);
            this.id = params.id;

            this.webReqservices.get("team/detaill/" + params.id).subscribe(
                (res) => {
                    console.log(res);
                    if (res["members"]) this.Members = res["members"];
                    console.log(this.Members);
                    this.webReqservices.get("users").subscribe(
                        (data) => {
                            // this.router.navigate(["/apps/images"]);

                            this.Users = data;
                            this.Users = this.Users.filter((elmm) => {
                                return this.Members.some(
                                    (elm) => elm == elmm.id
                                );
                            });
                            this.Userso = this.Users;
                            console.log("Users");
                            console.log(this.Users);
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
                },
                (err) => {
                    console.log(err);
                }
            );
        });
    }
    add() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";

        dialogConfig.data = {
            id: this.id,
            type: "create",
            members: this.Members,
        };
        let dialogRef = this.dialog.open(AddmemberComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                this.Members = this.Members.concat(result.members);
                this.webReqservices.get("users").subscribe(
                    (data) => {
                        // this.router.navigate(["/apps/images"]);

                        this.Users = data;
                        this.Users = this.Users.filter((elmm) => {
                            return this.Members.some((elm) => elm == elmm.id);
                        });
                        console.log("Users");
                        console.log(this.Users);
                        this._snackBar.open("Saved successfully", "Save", {
                            duration: 2000,
                        });
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }
    delete(id) {
        this.webReqservices
            .put("team/editmembers1/" + this.id, {
                members: [id],
            })
            .subscribe(
                (data) => {
                    console.log("data");

                    console.log(data);

                    this.Members = this.Members.filter((elm) => {
                        return elm != id;
                    });
                    this.Users = this.Users.filter((elmm) => {
                        return this.Members.some((elm) => elm == elmm.id);
                    });

                    this._snackBar.open("Deleted successfully", "Save", {
                        duration: 2000,
                    });
                },
                (error) => {
                    console.log(error);
                }
            );
    }
    search() {
        this.Users = this.Userso.filter((post) => {
            let name = post.name.toLowerCase();
            if (name.includes(this.searchKey.toLowerCase())) return true;
        });
    }
    vider() {
        this.searchKey = "";
        this.search();
    }
}
