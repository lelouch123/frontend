import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
@Component({
    selector: "app-resourceform",
    templateUrl: "./resourceform.component.html",
    styleUrls: ["./resourceform.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceformComponent implements OnInit {
    form: FormGroup;
    error: string;
    constructor(
        public dialogRef: MatDialogRef<ResourceformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        console.log(this.data);

        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            name: ["", Validators.required],
            description: ["", Validators.required],
            number: [
                "",
                [
                    Validators.required,
                    Validators.pattern("^[0-9]*$"),
                    Validators.minLength(1),
                ],
            ],
        });
        if (this.data["type"] != "create") {
            this.webReqservices
                .get("resource/detaill/" + this.data["id"])
                .subscribe(
                    (res) => {
                        console.log("res");
                        console.log(res);
                        this.form = this._formBuilder.group({
                            company: [
                                {
                                    value: "Focus",
                                    disabled: true,
                                },
                                Validators.required,
                            ],
                            name: [res["name"], Validators.required],
                            description: [
                                res["description"],
                                Validators.required,
                            ],
                            number: [
                                res["number"],
                                [
                                    Validators.required,
                                    Validators.pattern("^[0-9]*$"),
                                    Validators.minLength(1),
                                ],
                            ],
                        });
                    },
                    (err) => console.log(err)
                );
        }
    }
    onClick() {
        if (this.data["type"] == "create") {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices
                    .post("resource/create", {
                        name: role.name,
                        description: role.description,
                        number: role.number,
                    })
                    .subscribe(
                        (data) => {
                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "Resource successfully created";
                            this.dialogRef.close({
                                res: "success",
                                resource: data,
                            });
                        },
                        (error) => {
                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        } else {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices
                    .put("resource/edit/" + this.data["id"], {
                        name: role.name,
                        description: role.description,
                        number: role.number,
                    })
                    .subscribe(
                        (data) => {
                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "Resource successfully created";
                            this.dialogRef.close({
                                res: "success",
                                name: role.name,
                                description: role.description,
                                number: role.number,
                            });
                        },
                        (error) => {
                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
}
