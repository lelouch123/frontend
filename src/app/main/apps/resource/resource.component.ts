import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort, MatSnackBar } from "@angular/material";
import { RoleService } from "app/_services/role.service";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
import { ResourceformComponent } from "./resourceform/resourceform.component";
import { ShadowComponent } from "../shadow/shadow.component";

@Component({
    selector: "app-resource",
    templateUrl: "./resource.component.html",
    styleUrls: ["./resource.component.scss"],
})
export class ResourceComponent implements OnInit {
    displayedColumns: string[] = [
        "Company",
        "name",
        "description",
        "number",
        "actions",
    ];
    dataSource: MatTableDataSource<any>;
    resources: any = [];
    // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    searchKey: string = "";
    constructor(
        private rolservice: RoleService,
        private dialog: MatDialog,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar
    ) {}

    ngOnInit() {
        this.webReqservices.get("resource/all").subscribe(
            (data) => {
                // this.router.navigate(["/apps/images"]);
                this.resources = data;
                console.log(this.resources);
                this.dataSource = new MatTableDataSource<any>(this.resources);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Resource",
                        entityid: "all resources",
                        type: "read",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (error) => {
                console.log(error);
            }
        );

        // this.dataSource.sort = this.sort;
    }
    onSearchClear() {
        this.searchKey = "";
        this.applyFilter();
    }

    applyFilter() {
        this.dataSource.filter = this.searchKey.trim().toLowerCase();
    }

    editRole(row) {
        // this.dataSource = new MatTableDataSource<any>(this.rolservice.roles);
        // setTimeout(() => (this.dataSource.paginator = this.paginator));
        // setTimeout(() => (this.dataSource.sort = this.sort));
        // // console.log(row);
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { id: row.id, type: "edit" };
        let dialogRef = this.dialog.open(ResourceformComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                this.resources = this.resources.filter((value, key) => {
                    if (value.id == row.id) {
                        value.name = result.name;
                        value.description = result.description;
                        value.number = result.number;
                    }
                    return true;
                });
                this._snackBar.open("Saved successfully", "Save", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Resource",
                        entityid: row.id,
                        type: "update",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }
    create() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { type: "create" };
        let dialogRef = this.dialog.open(ResourceformComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                console.log("result.resource");
                console.log(result.resource);
                this.resources.push(result.resource);
                this.dataSource = new MatTableDataSource<any>(this.resources);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                this._snackBar.open("added successfully", "Add", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Resource",
                        entityid: result.resource.id,
                        type: "update",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }
    shadow(id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "30%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { id };
        dialogConfig.maxHeight = "90vh"; //you can adjust the value as per your view
        let dialogRef = this.dialog.open(ShadowComponent, dialogConfig);
    }
    deleteRole(row) {
        this.webReqservices.delete("resource/delete/" + row.id).subscribe(
            (res) => {
                this.resources = this.resources.filter((value, key) => {
                    return value.id != row.id;
                });

                this.dataSource = new MatTableDataSource<any>(this.resources);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                this._snackBar.open("Deleted successfully", "Delete", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Resource",
                        entityid: row.id,
                        type: "delete",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (err) => {
                console.log(err);
            }
        );

        // console.log(this.rolservice.roles);
    }
}
