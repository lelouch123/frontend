import {
    Component,
    ElementRef,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";

import { fuseAnimations } from "@fuse/animations";

import {
    MatTableDataSource,
    MatDialog,
    MatDialogConfig,
    MatSnackBar,
} from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
import { AddmemberComponent } from "../../team/addmember/addmember.component";
import { RoleformComponent } from "../../roles/roleform/roleform.component";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "e-commerce-orders",
    templateUrl: "./orders.component.html",
    styleUrls: ["./orders.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class EcommerceOrdersComponent implements OnInit {
    dataSource: MatTableDataSource<any>;
    displayedColumns = ["name", "description", "action"];
    searchKey: string;
    List: any;
    Listo: any;
    Pid: string = "";
    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild("filter", { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: false })
    sort: MatSort;
    constructor(
        private dialog: MatDialog,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar,
        private route: ActivatedRoute
    ) {}
    ngOnInit(): void {
        this.route.params.subscribe((params) => {
            this.Pid = params.id;
            console.log("this.Pid");
            console.log(this.Pid);
            this.webReqservices.get("project/" + params.id).subscribe(
                (data) => {
                    this.webReqservices
                        .get("project/board/" + data["templateId"])
                        .subscribe(
                            (response: any) => {
                                console.log("response");
                                var cards = [];
                                let card = {
                                    id: "",
                                    name: "",
                                    description: "",
                                    verfied: false,
                                };
                                var c = JSON.parse(response.board.obj);
                                if (c.cards) {
                                    if (data["actions"]) {
                                        console.log(data["actions"]);
                                        data["actions"].forEach((element) => {
                                            let el = c.cards.find(
                                                (k) => k.id == element.id
                                            );
                                            let card = {
                                                id: el.id,
                                                name: el.name,
                                                description: el.description,
                                                verfied: element.verfied,
                                            };
                                            // card.name = el.name;
                                            // card.id = el.id;
                                            // card.description = el.description;
                                            // card.verfied = element.verfied;
                                            cards.push(card);
                                        });
                                        console.log(cards);
                                        this.List = cards;
                                        //         this.Listo = data;
                                        //         console.log(this.List);
                                        this.dataSource = new MatTableDataSource<
                                            any
                                        >(this.List);
                                        setTimeout(
                                            () =>
                                                (this.dataSource.paginator = this.paginator)
                                        );
                                        setTimeout(
                                            () =>
                                                (this.dataSource.sort = this.sort)
                                        );
                                    }
                                }
                            },
                            (err) => {
                                console.log(err);
                            }
                        );
                },
                (err) => {
                    console.log(err);
                }
            );
        });
        // this.webReqservices.get("Shadow/all").subscribe(
        //     (data) => {
        //         this.List = data;
        //         this.Listo = data;
        //         console.log(this.List);
        //         this.dataSource = new MatTableDataSource<any>(this.List);
        //         setTimeout(() => (this.dataSource.paginator = this.paginator));
        //         setTimeout(() => (this.dataSource.sort = this.sort));
        //     },
        //     (error) => {
        //         console.log(error);
        //     }
        // );
    }
    verfie(id, verf) {
        console.log(id);
        console.log(verf);
        this.List.forEach((element, i) => {
            if (element.id == id) {
                this.List[i].verfied = verf;
            }
        });

        this.webReqservices
            .put("project/verfication/" + this.Pid + "/" + id, { boo: verf })
            .subscribe(
                (res) => console.log(res),
                (err) => console.log(err)
            );
    }
    // search() {
    //     this.List = this.Listo.filter((post) => {
    //         if (post.user != null) {
    //             let name = post.user.toLowerCase();
    //             if (name.includes(this.searchKey.toLowerCase())) return true;
    //         }
    //         if (post.date != null) {
    //             let date = post.date.toLowerCase();
    //             if (date.includes(this.searchKey.toLowerCase())) return true;
    //         }
    //         if (post.type != null) {
    //             let type = post.type.toLowerCase();
    //             if (type.includes(this.searchKey.toLowerCase())) return true;
    //         }
    //         if (post.type != null) {
    //             let entity = post.entity.toLowerCase();
    //             if (entity.includes(this.searchKey.toLowerCase())) return true;
    //         }
    //     });
    //     this.dataSource = new MatTableDataSource<any>(this.List);
    //     setTimeout(() => (this.dataSource.paginator = this.paginator));
    //     setTimeout(() => (this.dataSource.sort = this.sort));
    // }
}
