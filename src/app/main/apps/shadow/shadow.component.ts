import { Component, OnInit, Inject, ViewEncapsulation } from "@angular/core";
import { WebRequestService } from "app/_services/web-request.service";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { fuseAnimations } from "@fuse/animations";
@Component({
    selector: "app-shadow",
    templateUrl: "./shadow.component.html",
    styleUrls: ["./shadow.component.css"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class ShadowComponent implements OnInit {
    d: Date = new Date();
    month: any = new Array();
    n: string = "";
    groups: any = {};
    id: string = "5eba4779d48dbe4374e580fe";
    entries: Number = 0;
    data1: any = [
        {
            id: "5ebcb809f894ff440cd4ddb8",
            entity: "project",
            entityid: "5eba4779d48dbe4374e580fe",
            type: "edit",
            user: null,
            date: "2020-05-14T03:16:25.989Z",
        },
        {
            id: "5ebcb85cf894ff440cd4ddb9",
            entity: "project",
            entityid: "5eba4779d48dbe4374e580fe",
            type: "edit",
            user: null,
            date: "2020-05-14T03:17:48.203Z",
        },
        {
            id: "5ebcba4bf894ff440cd4ddba",
            entity: "project",
            entityid: "5eba4779d48dbe4374e580fe",
            type: "edit",
            user: "ala1",
            date: "2020-05-15T03:26:03.343Z",
        },
    ];
    constructor(
        private webReqservices: WebRequestService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        public dialogRef: MatDialogRef<ShadowComponent>
    ) {}

    ngOnInit() {
        this.dialogRef.disableClose = true;
        console.log("this.data");

        console.log(this.data);
        this.month[0] = "January";
        this.month[1] = "February";
        this.month[2] = "March";
        this.month[3] = "April";
        this.month[4] = "May";
        this.month[5] = "June";
        this.month[6] = "July";
        this.month[7] = "August";
        this.month[8] = "September";
        this.month[9] = "October";
        this.month[10] = "November";
        this.month[11] = "December";
        this.n = this.month[this.d.getMonth()];
        console.log(JSON.parse(localStorage.getItem("currentUser")).name);

        /// grouping the shadows

        /* Shadowing operation */
        this.webReqservices.get("shadow/" + this.data["id"]).subscribe(
            (res) => {
                console.log("shadowing");
                console.log(res);
                this.data1 = res;
                this.groups = this.data1.reduce((groups, game) => {
                    const date = game.date.split("T")[0];

                    if (!groups[date]) {
                        groups[date] = [];
                    }
                    groups[date].push(game);
                    return groups;
                }, {});
                this.entries = Object.keys(this.groups).length;

                console.log(this.groups);
            },
            (err) => console.log(err)
        );
        /* End of shadowing operation */
    }

    getday(s) {
        let days = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
        ];
        let d = new Date(s);
        let dayName = days[d.getDay() - 1];
        return dayName;
    }
    /* Shadowing operation */
    //    this.webReqservices
    //    .post("shadow/", {
    //        entity: "project",
    //        entityid: this.id,
    //        type: "edit",
    //        user: JSON.parse(localStorage.getItem("currentUser")).name,
    //    })
    //    .subscribe(
    //        (res) => {
    //            console.log("shadowing");
    //            console.log(res);
    //        },
    //        (err) => console.log(err)
    //    );
    /* End of shadowing operation */
}
