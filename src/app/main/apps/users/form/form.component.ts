import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
import { HttpClient, HttpEventType } from "@angular/common/http";

@Component({
    selector: "app-form",
    templateUrl: "./form.component.html",
    styleUrls: ["./form.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
    form: FormGroup;
    error: string;
    roles1: any = [];
    constructor(
        public dialogRef: MatDialogRef<FormComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private _snackBar: MatSnackBar,
        private http: HttpClient
    ) {}

    ngOnInit() {
        console.log(this.data);

        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            name: ["", Validators.required],
            lastname: ["", Validators.required],
            email: ["", Validators.email],
            age: [
                "",
                [
                    Validators.required,
                    Validators.pattern("^[0-9]*$"),
                    Validators.minLength(1),
                ],
            ],
            password: ["", Validators.required],
            roles: new FormControl(""),
            image: [""],
        });
        this.rolservice.getroles().subscribe(
            (rols) => {
                console.log("res1");
                console.log(rols);
                this.roles1 = rols;
                this.form = this._formBuilder.group({
                    company: [
                        {
                            value: "Focus",
                            disabled: true,
                        },
                        Validators.required,
                    ],
                    name: ["", Validators.required],
                    lastname: ["", Validators.required],
                    email: ["", Validators.email],
                    age: [
                        "",
                        [
                            Validators.required,
                            Validators.pattern("^[0-9]*$"),
                            Validators.minLength(1),
                        ],
                    ],
                    password: ["", Validators.required],
                    roles: new FormControl(rols[0].role),
                    image: [""],
                });
            },
            (error) => {
                console.log(error);
            }
        );
        if (this.data["type"] != "create") {
            this.webReqservices
                .get("users/detaill/" + this.data["id"])
                .subscribe(
                    (res) => {
                        console.log("res");
                        console.log(res);

                        this.rolservice.getroles().subscribe(
                            (rols) => {
                                console.log("res1");
                                console.log(rols);
                                this.roles1 = rols;
                                this.form = this._formBuilder.group({
                                    company: [
                                        {
                                            value: "Focus",
                                            disabled: true,
                                        },
                                        Validators.required,
                                    ],
                                    name: [res["name"], Validators.required],
                                    lastname: [
                                        res["lastname"],
                                        Validators.required,
                                    ],
                                    email: [res["email"], Validators.email],
                                    age: [
                                        res["age"],
                                        [
                                            Validators.required,
                                            Validators.pattern("^[0-9]*$"),
                                            Validators.minLength(1),
                                        ],
                                    ],
                                    password: [
                                        res["password"],
                                        Validators.required,
                                    ],
                                    roles: new FormControl(
                                        res["roles"][0].title
                                    ),
                                    image: new FormControl(res["image"]),
                                });
                            },
                            (error) => {
                                console.log(error);
                            }
                        );
                    },
                    (err) => console.log(err)
                );
        }
    }
    onClick() {
        if (this.data["type"] == "create") {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var FormValue = this.form.getRawValue();
                // { role: role["role"] }
                console.log("JSON.stringify(FormValue)");

                console.log(JSON.stringify(FormValue));

                this.webReqservices
                    .post("users/create", {
                        name: FormValue.name,
                        lastname: FormValue.lastname,
                        age: FormValue.age,
                        password: FormValue.password,
                        email: FormValue.email,
                        roles: [{ title: FormValue.roles }],
                        image: FormValue.image,
                    })
                    .subscribe(
                        (data) => {
                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "User successfully created";
                            this.dialogRef.close({
                                res: "success",
                                resource: data,
                            });
                        },
                        (error) => {
                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        } else {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var FormValue = this.form.getRawValue();

                // { role: role["role"] }
                console.log(JSON.stringify(FormValue));

                this.webReqservices
                    .put("users/edit/" + this.data["id"], {
                        name: FormValue.name,
                        lastname: FormValue.lastname,
                        age: FormValue.age,
                        password: FormValue.password,
                        email: FormValue.email,
                        roles: [{ title: FormValue.roles }],
                        image: FormValue.image,
                    })
                    .subscribe(
                        (data) => {
                            console.log("data");

                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "User successfully Updated";
                            this.dialogRef.close({
                                res: "success",
                                resource: data,
                            });
                        },
                        (error) => {
                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
        console.log("this.form.getRawValue()");
        console.log(this.form.getRawValue());
    }

    public uploadFile = (files) => {
        if (files.length === 0) {
            return;
        }
        console.log("files[0]");
        console.log(files[0]);
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append("file", fileToUpload, fileToUpload.name);

        this.http
            .post("http://localhost:4000/project/images", formData, {
                reportProgress: true,
                observe: "events",
            })
            .subscribe((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    // this.progress = Math.round(
                    //     (100 * event.loaded) / event.total
                    // );
                } else if (event.type === HttpEventType.Response) {
                    // this.message = "Upload success.";
                    console.log(event.body["dbPath"]);
                    this._snackBar.open("Uploaded successfully", "Upload", {
                        duration: 2000,
                    });
                    this.form.patchValue({
                        image: event.body["dbPath"],
                    });
                }
            });

        // this.webReqservices.post("project/images", formData).subscribe(
        //     (res) => console.log(res),

        //     (err) => console.log(err)
        // );
    };
}
