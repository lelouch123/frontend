import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { UsersComponent } from "./users.component";
import { FormComponent } from "./form/form.component";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";
import {
    MatTableModule,
    MatSort,
    MatSortModule,
    MatDialogModule,
} from "@angular/material";
import { MatPaginatorModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { MatRadioModule } from "@angular/material";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { FuseSharedModule } from "@fuse/shared.module";

const routes: Routes = [{ path: "", component: UsersComponent }];

@NgModule({
    declarations: [UsersComponent, FormComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        MatSortModule,
        MatRadioModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatDialogModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseSharedModule,
    ],
    entryComponents: [FormComponent],
})
export class UsersModule {}
