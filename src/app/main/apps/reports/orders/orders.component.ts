import {
    Component,
    ElementRef,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";

import { fuseAnimations } from "@fuse/animations";

import {
    MatTableDataSource,
    MatDialog,
    MatDialogConfig,
    MatSnackBar,
} from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
import { AddmemberComponent } from "../../team/addmember/addmember.component";
import { RoleformComponent } from "../../roles/roleform/roleform.component";
import { LocationHubService } from "app/_services/location-hub.service";

@Component({
    selector: "e-commerce-orders",
    templateUrl: "./orders.component.html",
    styleUrls: ["./orders.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class EcommerceOrdersComponent implements OnInit {
    dataSource: MatTableDataSource<any>;
    displayedColumns = ["report", "description", "senderid", "date", "type"];
    searchKey: string;
    List: any;
    Listo: any;
    users: any = [];
    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild("filter", { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: false })
    sort: MatSort;
    constructor(
        private dialog: MatDialog,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar,
        private location: LocationHubService
    ) {}
    ngOnInit(): void {
        this.location.reports.subscribe(
            (msg) => {
                console.log(msg);
                this.List.push(msg);
                this.Listo = this.List;
                console.log(this.List);
                this.dataSource = new MatTableDataSource<any>(this.List);
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
            },
            (err) => console.log(err)
        );
        this.webReqservices.get("users").subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.users = res;
                this.webReqservices
                    .get(
                        "report/all/" +
                            JSON.parse(localStorage.getItem("currentUser")).id
                    )
                    .subscribe(
                        (data) => {
                            this.List = data;
                            this.Listo = data;
                            console.log(this.List);
                            this.dataSource = new MatTableDataSource<any>(
                                this.List
                            );
                            setTimeout(
                                () =>
                                    (this.dataSource.paginator = this.paginator)
                            );
                            setTimeout(
                                () => (this.dataSource.sort = this.sort)
                            );
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
            },
            (err) => {
                console.log(err);
            }
        );
    }
    getUsername(id) {
        var user = this.users.find((elm) => elm.id == id);
        return user.name;
    }
    search() {
        this.List = this.Listo.filter((post) => {
            if (post.report != null) {
                let name = post.report.toLowerCase();
                if (name.includes(this.searchKey.toLowerCase())) return true;
            }
            if (post.date != null) {
                let date = post.date.toLowerCase();
                if (date.includes(this.searchKey.toLowerCase())) return true;
            }
            if (post.type != null) {
                let type = post.type.toLowerCase();
                if (type.includes(this.searchKey.toLowerCase())) return true;
            }
        });
        this.dataSource = new MatTableDataSource<any>(this.List);
        setTimeout(() => (this.dataSource.paginator = this.paginator));
        setTimeout(() => (this.dataSource.sort = this.sort));
    }
}
