import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { ShadowComponent } from "./shadow.component";

import {
    MatTableModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
} from "@angular/material";

import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";
import { FuseSharedModule } from "@fuse/shared.module";
import { MatChipsModule } from "@angular/material/chips";
import { MatDatepickerModule } from "@angular/material/datepicker";

import { MatSnackBarModule } from "@angular/material/snack-bar";
import { FormsModule } from "@angular/forms";

import { MatDividerModule } from "@angular/material/divider";
import { FuseWidgetModule } from "@fuse/components";
import { EcommerceOrdersComponent } from "./orders/orders.component";

const routes: Routes = [
    {
        path: "reports",
        component: EcommerceOrdersComponent,
    },
];

@NgModule({
    declarations: [EcommerceOrdersComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseSharedModule,
        MatChipsModule,
        MatDatepickerModule,
        MatSnackBarModule,
        FormsModule,
        MatDividerModule,
        FuseWidgetModule,
        MatTableModule,
        MatDialogModule,
        MatSortModule,
        MatPaginatorModule,
    ],
})
export class ShadowModule {}
