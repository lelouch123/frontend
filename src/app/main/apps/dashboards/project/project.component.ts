import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable } from "rxjs";
import * as shape from "d3-shape";

import { fuseAnimations } from "@fuse/animations";

import { ProjectDashboardService } from "app/main/apps/dashboards/project/project.service";
import { FuseSidebarService } from "@fuse/components/sidebar/sidebar.service";
import { WebRequestService } from "app/_services/web-request.service";
import { MatTableDataSource } from "@angular/material";

@Component({
    selector: "project-dashboard",
    templateUrl: "./project.component.html",
    styleUrls: ["./project.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class ProjectDashboardComponent implements OnInit {
    projects: any[];
    selectedProject: any;
    team: any = [];
    name: any = "";
    myteams: any = [];
    widgets: any;
    widget5: any = {};
    widget6: any = {};
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};
    myprojects: any = [];
    dataSource: MatTableDataSource<any>;
    displayedColumns = ["image", "name", "lastname", "email"];
    dateNow = Date.now();

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {ProjectDashboardService} _projectDashboardService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _projectDashboardService: ProjectDashboardService,
        private webReqservices: WebRequestService
    ) {
        /**
         * Widget 5
         */
        this.widget5 = {
            currentRange: "TW",
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: "Days",
            showYAxisLabel: false,
            yAxisLabel: "Isues",
            scheme: {
                domain: ["#42BFF7", "#C6ECFD", "#C7B42C", "#AAAAAA"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: "",
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: "Days",
                showYAxisLabel: false,
                yAxisLabel: "Isues",
                scheme: {
                    domain: ["#42BFF7", "#C6ECFD", "#C7B42C", "#AAAAAA"],
                },
                curve: shape.curveBasis,
            },
        };

        /**
         * Widget 6
         */
        this.widget6 = {
            currentRange: "TW",
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: true,
            gradient: false,
            scheme: {
                domain: ["#f44336", "#9c27b0", "#03a9f4", "#e91e63"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
        };

        /**
         * Widget 7
         */
        this.widget7 = {
            currentRange: "T",
        };

        /**
         * Widget 8
         */
        this.widget8 = {
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: false,
            gradient: false,
            scheme: {
                domain: ["#f44336", "#9c27b0", "#03a9f4", "#e91e63", "#ffc107"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
        };

        /**
         * Widget 9
         */
        this.widget9 = {
            currentRange: "TW",
            xAxis: false,
            yAxis: false,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: "Days",
            showYAxisLabel: false,
            yAxisLabel: "Isues",
            scheme: {
                domain: ["#42BFF7", "#C6ECFD", "#C7B42C", "#AAAAAA"],
            },
            curve: shape.curveBasis,
        };

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.projects = this._projectDashboardService.projects;

        this.widgets = this._projectDashboardService.widgets;
        this.webReqservices.get("team/all").subscribe(
            (data: any) => {
                var id = JSON.parse(localStorage.getItem("currentUser")).id;
                console.log("id");
                console.log(id);
                var myteam = [];
                data.forEach((element) => {
                    console.log(element.members);
                    if (element.members.includes(id)) {
                        myteam.push(...element.members);
                        this.myteams.push(element.id);
                    }
                });
                console.log("myteam");

                console.log(myteam);
                console.log(this.myteams);
                this.webReqservices.get("project/all").subscribe(
                    (res: any) => {
                        console.log("projects");

                        console.log(res);
                        res.forEach((element) => {
                            if (
                                this.myteams.some((r) =>
                                    element.teams.includes(r)
                                )
                            )
                                this.myprojects.push(element);
                        });
                        console.log("this.myprojects");
                        console.log(this.myprojects);
                        this.selectedProject = this.myprojects[0];
                    },
                    (err) => console.log(err)
                );

                this.webReqservices.get("users").subscribe(
                    (users: any) => {
                        users.forEach((el) => {
                            if (myteam.includes(id)) {
                                this.team.push(el);
                            }
                        });
                        console.log("this.team");

                        console.log(this.team);
                        this.dataSource = new MatTableDataSource<any>(
                            this.team
                        );
                    },
                    (err) => console.log(err)
                );
            },
            (err) => console.log(err)
        );

        /**
         * Widget 11
         */
        this.widget11.onContactsChanged = new BehaviorSubject({});
        this.widget11.onContactsChanged.next(this.widgets.widget11.table.rows);
        this.widget11.dataSource = new FilesDataSource(this.widget11);
        this.name = JSON.parse(localStorage.getItem("currentUser")).name;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param _widget11
     */
    constructor(private _widget11) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._widget11.onContactsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {}
}
