import { Component, OnInit, ViewEncapsulation } from "@angular/core";

import { fuseAnimations } from "@fuse/animations";

import { AnalyticsDashboardService } from "app/main/apps/dashboards/analytics/analytics.service";
import { WebRequestService } from "app/_services/web-request.service";

@Component({
    selector: "analytics-dashboard",
    templateUrl: "./analytics.component.html",
    styleUrls: ["./analytics.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class AnalyticsDashboardComponent implements OnInit {
    widgets: any;
    widget1SelectedYear = "2016";
    widget5SelectedDay = "today";
    currentyear: any = "";

    /**
     * Constructor
     *
     * @param {AnalyticsDashboardService} _analyticsDashboardService
     */
    constructor(
        private _analyticsDashboardService: AnalyticsDashboardService,
        private webReqservices: WebRequestService
    ) {
        // Register the custom chart.js plugin
        this._registerCustomChartJSPlugin();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.webReqservices.get("project/all").subscribe(
            (res: any) => {
                console.log("res");
                console.log(res);
                //this year
                let list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var d = new Date();
                var year = d.getFullYear();
                var month = d.getMonth();
                list.forEach((element, i) => {
                    res.forEach((project) => {
                        var m = new Date(project.startdate).getMonth();
                        var y = new Date(project.startdate).getFullYear();
                        if (y == year && m == i) {
                            list[i]++;
                            console.log("list");
                        }
                    });
                });
                console.log(list);
                this.widgets.widget1.datasets["2016"][0].data = list;
                //this year-1
                list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var d = new Date();
                var year = d.getFullYear() - 1;
                var month = d.getMonth();
                list.forEach((element, i) => {
                    res.forEach((project) => {
                        var m = new Date(project.startdate).getMonth();
                        var y = new Date(project.startdate).getFullYear();
                        if (y == year && m == i) {
                            list[i]++;
                            console.log("list");
                        }
                    });
                });
                console.log(list);
                this.widgets.widget1.datasets["2017"][0].data = list;
                //this year-2
                list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var d = new Date();
                var year = d.getFullYear() - 2;
                var month = d.getMonth();
                list.forEach((element, i) => {
                    res.forEach((project) => {
                        var m = new Date(project.startdate).getMonth();
                        var y = new Date(project.startdate).getFullYear();
                        if (y == year && m == i) {
                            list[i]++;
                            console.log("list");
                        }
                    });
                });
                console.log(list);
                this.widgets.widget1.datasets["2018"][0].data = list;
            },
            (err) => {
                console.log(err);
            }
        );

        this.webReqservices.get("resource/all").subscribe(
            (data: any) => {
                data.sort((a, b) => (a.number < b.number ? 1 : -1));
                console.log(data);
                this.widgets.widget2.labels = [];
                this.widgets.widget2.datasets[0].data[0] = 0;
                this.widgets.widget2.conversion.value = 0;
                this.widgets.widget2.datasets[0].data[1] = 0;
                this.widgets.widget2.datasets[0].data[2] = 0;
                this.widgets.widget2.datasets[0].data[3] = 0;
                this.widgets.widget2.datasets[0].data[4] = 0;
                this.widgets.widget2.datasets[0].data[5] = 0;
                this.widgets.widget2.datasets[0].data[6] = 0;
                data.forEach((element, index) => {
                    this.widgets.widget2.labels[index] = element.name;
                    this.widgets.widget2.datasets[0].data[index] =
                        element.number;
                    this.widgets.widget2.conversion.value += element.number;
                });
            },
            (err) => {
                console.log(err);
            }
        );
        this.webReqservices.get("project/all/tasks").subscribe(
            (data: any) => {
                var d = new Date();
                var year = d.getFullYear();
                var month = d.getMonth();
                var day = d.getDate();
                console.log("days");

                var days = this.Last7Days();
                console.log(days);
                var list = [0, 0, 0, 0, 0, 0, 0];
                var total = 0;
                //widget5
                var list1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var total1 = 0;
                //widget 5
                data.boards.forEach((board) => {
                    var cards = JSON.parse(board.obj);
                    if (cards.cards) {
                        board.cards = cards.cards;
                        board.cards.forEach((element) => {
                            if (element.due != "") {
                                var m = new Date(element.due).getMonth();
                                var y = new Date(element.due).getFullYear();
                                var d = new Date(element.due).getDate();
                                //widget 5

                                //widget 5

                                if (
                                    year === y &&
                                    m === days[0].getMonth() &&
                                    d === days[0].getDate()
                                ) {
                                    var h = new Date(element.due).getHours();
                                    console.log("h");
                                    console.log(h);
                                    if (h > 22) {
                                        list1[0]++;
                                    }
                                    if (h >= 0 && h < 2) {
                                        list1[1]++;
                                    }
                                    if (h >= 2 && h < 4) {
                                        list1[2]++;
                                    }
                                    if (h >= 4 && h < 6) {
                                        list1[3]++;
                                    }
                                    if (h >= 6 && h < 8) {
                                        list1[4]++;
                                    }
                                    if (h >= 8 && h < 10) {
                                        list1[5]++;
                                    }
                                    if (h >= 10 && h < 12) {
                                        list1[6]++;
                                    }
                                    if (h >= 12 && h < 14) {
                                        list1[7]++;
                                    }
                                    if (h >= 14 && h < 16) {
                                        list1[8]++;
                                    }
                                    if (h >= 16 && h < 18) {
                                        list1[9]++;
                                    }
                                    if (h >= 18 && h < 20) {
                                        list1[10]++;
                                    }
                                    if (h >= 20 && h < 22) {
                                        list1[11]++;
                                    }
                                }
                            }
                        });
                        list.forEach((l, index) => {
                            board.cards.forEach((element) => {
                                if (element.due != "") {
                                    var m = new Date(element.due).getMonth();
                                    var y = new Date(element.due).getFullYear();
                                    var d = new Date(element.due).getDate();
                                    if (
                                        year === y &&
                                        m == days[index].getMonth() &&
                                        d == days[index].getDate()
                                    ) {
                                        console.log("true");
                                        list[index] += 1;
                                        total++;
                                    }
                                }
                            });
                        });
                        console.log("list1");
                        console.log(list1);
                    }
                });
                this.webReqservices.get("project/all").subscribe(
                    (res: any) => {
                        res.forEach((proejct, index) => {
                            res[index].tasks = 0;
                            data.boards.forEach((board) => {
                                if (board.projectId == proejct.id) {
                                    console.log("hello");
                                    if (board.cards)
                                        res[index].tasks = board.cards.length;
                                }
                            });
                        });
                        console.log("total1");
                        console.log(total1);
                        res.sort((a, b) => (a.tasks < b.tasks ? 1 : -1));
                        this.widgets.widget7.devices[0].name = res[0].title;
                        this.widgets.widget7.devices[0].value = res[0].tasks;
                        this.widgets.widget7.devices[1].name = res[1].title;
                        this.widgets.widget7.devices[1].value = res[1].tasks;
                        this.widgets.widget7.devices[2].name = res[2].title;
                        this.widgets.widget7.devices[2].value = res[2].tasks;
                    },
                    (err) => console.log(err)
                );
                console.log(list);

                console.log(data);
                this.widgets.widget3.labels = [];
                days.forEach((elm, index) => {
                    var date = elm.getDate();
                    var month = elm.getMonth() + 1;
                    var year = elm.getFullYear();
                    var dateStr = date + "/" + month + "/" + year;
                    this.widgets.widget3.labels[index] = dateStr;
                });

                this.widgets.widget3.datasets[0].data = [];
                this.widgets.widget3.datasets[0].data = list;
                this.widgets.widget3.total = total;
            },
            (err) => {
                console.log(err);
            }
        );

        this.webReqservices.get("team/all").subscribe(
            (data: any) => {
                console.log("team");
                console.log(data);
                this.widgets.widget4.labels = [];
                this.widgets.widget4.datasets[0].data[0] = 0;
                this.widgets.widget4.visits.value = 0;
                this.widgets.widget4.datasets[0].data[1] = 0;
                this.widgets.widget4.datasets[0].data[2] = 0;
                this.widgets.widget4.datasets[0].data[3] = 0;
                this.widgets.widget4.datasets[0].data[4] = 0;
                this.widgets.widget4.datasets[0].data[5] = 0;
                this.widgets.widget4.datasets[0].data[6] = 0;
                data.forEach((element, index) => {
                    this.widgets.widget4.visits.value += 1;
                    this.widgets.widget4.datasets[0].data[index] =
                        element.number;
                    this.widgets.widget4.labels[index] = element.name;
                });
            },
            (err) => {
                console.log(err);
            }
        );
        // Get the widgets from the service
        var d = new Date();
        var n = d.getFullYear();

        this.currentyear = n;
        this.widgets = this._analyticsDashboardService.widgets;
        console.log(" this.widgets ");
        console.log(this.widgets);

        // this.widgets.widget1.datasets["2016"][0].data = [
        //     1,
        //     4,
        //     8,
        //     14,
        //     23,
        //     30,
        //     32,
        //     33,
        //     44,
        //     50,
        //     51,
        //     60,
        // ];

        //widget 2
        // this.widgets.widget2.labels = [
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data7",
        // ];
        // this.widgets.widget2.datasets[0].data[0] = 15;
        // this.widgets.widget2.conversion.value = 200;
        // this.widgets.widget2.datasets[0].data[1] = 20;
        // this.widgets.widget2.datasets[0].data[2] = 40;
        // this.widgets.widget2.datasets[0].data[3] = 30;
        // this.widgets.widget2.datasets[0].data[4] = 35;
        // this.widgets.widget2.datasets[0].data[5] = 20;
        // this.widgets.widget2.datasets[0].data[6] = 30;

        //widget 3
        // this.widgets.widget3.labels = [
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data7",
        // ];
        // this.widgets.widget3.datasets[0].data = [];
        // this.widgets.widget3.datasets[0].data[0] = 15;

        // this.widgets.widget3.datasets[0].data[1] = 20;
        // this.widgets.widget3.datasets[0].data[2] = 40;
        // this.widgets.widget3.datasets[0].data[3] = 30;
        // this.widgets.widget3.datasets[0].data[4] = 35;
        // this.widgets.widget3.datasets[0].data[5] = 20;
        // this.widgets.widget3.datasets[0].data[6] = 30;

        //widget 4
        // this.widgets.widget4.labels = [
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data1",
        //     "data2",
        //     "data7",
        // ];
        // this.widgets.widget4.datasets[0].data[0] = 30;
        // this.widgets.widget4.visits.value = 200;
        // this.widgets.widget4.datasets[0].data[1] = 50;
        // this.widgets.widget4.datasets[0].data[2] = 40;
        // this.widgets.widget4.datasets[0].data[3] = 30;
        // this.widgets.widget4.datasets[0].data[4] = 35;
        // this.widgets.widget4.datasets[0].data[5] = 20;
        // this.widgets.widget4.datasets[0].data[6] = 30;
        //widget 7

        this.widgets.widget7.devices[0].name = "0";
        this.widgets.widget7.devices[0].value = 0;
        this.widgets.widget7.devices[1].name = "0";
        this.widgets.widget7.devices[1].value = 0;
        this.widgets.widget7.devices[2].name = "0";
        this.widgets.widget7.devices[2].value = 0;
        //widget 5
        this.widgets.widget5.datasets.today[1] = [];
        this.widgets.widget5.datasets.yesterday[1] = [];

        this.widgets.widget5.datasets.today[0].label = "actions";
        this.widgets.widget5.datasets.today[0].data[0] = 3;
        this.widgets.widget5.datasets.today[0].data[1] = 3;
        this.widgets.widget5.datasets.today[0].data[2] = 10;
        this.widgets.widget5.datasets.today[0].data[3] = 6;
        this.widgets.widget5.datasets.today[0].data[4] = 5;
        this.widgets.widget5.datasets.today[0].data[5] = 2;
        this.widgets.widget5.datasets.today[0].data[6] = 3;
        this.widgets.widget5.datasets.today[0].data[7] = 6;
        this.widgets.widget5.datasets.today[0].data[8] = 8;
        this.widgets.widget5.datasets.today[0].data[9] = 11;
        this.widgets.widget5.datasets.today[0].data[10] = 3;
        this.widgets.widget5.datasets.today[0].data[11] = 2;

        this.widgets.widget5.datasets.yesterday[0].label = "actions";
        this.widgets.widget5.datasets.yesterday[0].data[0] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[1] = 4;
        this.widgets.widget5.datasets.yesterday[0].data[2] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[3] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[4] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[5] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[6] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[7] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[8] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[9] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[10] = 3;
        this.widgets.widget5.datasets.yesterday[0].data[11] = 2;
    }
    Last7Days() {
        var result = [];
        for (var i = 0; i < 7; i++) {
            var d = new Date();
            d.setDate(d.getDate() - i);
            result.push(d);
        }

        return result;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register a custom plugin
     */
    private _registerCustomChartJSPlugin(): void {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop &&
                        chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = "rgba(255, 255, 255, 0.7)";
                            const fontSize = 13;
                            const fontStyle = "normal";
                            const fontFamily = "Roboto, Helvetica Neue, Arial";
                            ctx.font = (window as any).Chart.helpers.fontString(
                                fontSize,
                                fontStyle,
                                fontFamily
                            );

                            // Just naively convert to string for now
                            const dataString =
                                dataset.data[index].toString() + "k";

                            // Make sure alignment settings are correct
                            ctx.textAlign = "center";
                            ctx.textBaseline = "middle";
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = "rgba(255,255,255,0.12)";
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            },
        });
    }
}
