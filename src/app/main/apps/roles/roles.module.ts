import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { RolesComponent } from "./roles.component";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";
import {
    MatTableModule,
    MatSort,
    MatSortModule,
    MatDialogModule,
} from "@angular/material";
import { MatPaginatorModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { PermissionComponent } from "./permission/permission.component";
import { MatRadioModule } from "@angular/material";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { RoleformComponent } from "./roleform/roleform.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { ShadowComponent } from "../shadow/shadow.component";

const routes: Routes = [
    { path: "", component: RolesComponent },
    { path: "permission", component: PermissionComponent },
];

@NgModule({
    declarations: [RolesComponent, PermissionComponent, RoleformComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
        MatSortModule,
        MatRadioModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatDialogModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseSharedModule,
    ],
    entryComponents: [RoleformComponent],
})
export class RolesModule {}
