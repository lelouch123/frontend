import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort, MatSnackBar } from "@angular/material";
import { RoleService } from "app/_services/role.service";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { RoleformComponent } from "./roleform/roleform.component";
import { WebRequestService } from "app/_services/web-request.service";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { ShadowComponent } from "../shadow/shadow.component";

@Component({
    selector: "app-roles",
    templateUrl: "./roles.component.html",
    styleUrls: ["./roles.component.scss"],
})
export class RolesComponent implements OnInit {
    displayedColumns: string[] = ["Company", "role", "description", "actions"];
    dataSource: MatTableDataSource<any>;

    // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    searchKey: string = "";
    constructor(
        private rolservice: RoleService,
        private dialog: MatDialog,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar
    ) {}

    ngOnInit() {
        this.rolservice.getroles().subscribe(
            (data) => {
                // this.router.navigate(["/apps/images"]);
                this.rolservice.roles = data;
                console.log(this.rolservice.roles);
                this.dataSource = new MatTableDataSource<any>(
                    this.rolservice.roles
                );
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Role",
                        entityid: "all role",
                        type: "read",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (error) => {
                console.log(error);
            }
        );

        // this.dataSource.sort = this.sort;
    }
    onSearchClear() {
        this.searchKey = "";
        this.applyFilter();
    }

    applyFilter() {
        this.dataSource.filter = this.searchKey.trim().toLowerCase();
    }

    editRole(row) {
        this.dataSource = new MatTableDataSource<any>(this.rolservice.roles);
        setTimeout(() => (this.dataSource.paginator = this.paginator));
        setTimeout(() => (this.dataSource.sort = this.sort));
        // console.log(row);
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        dialogConfig.panelClass = "custom-dialog-container";

        dialogConfig.data = { id: row.id };
        let dialogRef = this.dialog.open(RoleformComponent, dialogConfig);
        dialogRef.afterClosed().subscribe((result) => {
            if (result.res == "success") {
                console.log("sucess");
                this.rolservice.roles = this.rolservice.roles.filter(
                    (value, key) => {
                        if (value.id == row.id) {
                            value.role = result.role;
                            value.description = result.description;
                        }
                        return true;
                    }
                );
                this._snackBar.open("Saved successfully", "Save", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Role",
                        entityid: row.id,
                        type: "update",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            } else if (result.res == "err") {
                console.log("err");
            } else if (result.res == "cancel") {
                console.log("cancel");
            }
        });
    }

    shadow(id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "30%";
        dialogConfig.panelClass = "custom-dialog-container";
        dialogConfig.data = { id };
        dialogConfig.maxHeight = "90vh"; //you can adjust the value as per your view
        let dialogRef = this.dialog.open(ShadowComponent, dialogConfig);
    }
    deleteRole(row) {
        this.webReqservices.delete("users/roledelete/" + row.id).subscribe(
            (res) => {
                this.rolservice.roles = this.rolservice.roles.filter(
                    (value, key) => {
                        return value.id != row.id;
                    }
                );

                this.dataSource = new MatTableDataSource<any>(
                    this.rolservice.roles
                );
                setTimeout(() => (this.dataSource.paginator = this.paginator));
                setTimeout(() => (this.dataSource.sort = this.sort));
                this._snackBar.open("Deleted successfully", "Delete", {
                    duration: 2000,
                });
                /* Shadowing operation */
                this.webReqservices
                    .post("shadow/", {
                        entity: "Role",
                        entityid: row.id,
                        type: "delete",
                        user: JSON.parse(localStorage.getItem("currentUser"))
                            .name,
                    })
                    .subscribe(
                        (res) => {
                            console.log("shadowing");
                            console.log(res);
                        },
                        (err) => console.log(err)
                    );
                /* End of shadowing operation */
            },
            (err) => {
                console.log(err);
            }
        );

        // console.log(this.rolservice.roles);
    }
}
