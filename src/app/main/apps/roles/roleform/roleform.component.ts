import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";

@Component({
    selector: "app-roleform",
    templateUrl: "./roleform.component.html",
    styleUrls: ["./roleform.component.scss"],
})
export class RoleformComponent implements OnInit {
    form: FormGroup;
    error: string;
    constructor(
        public dialogRef: MatDialogRef<RoleformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        console.log(this.data);
        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            role: ["", Validators.required],
            description: ["", Validators.required],
        });
        this.webReqservices.get("users/onerole/" + this.data["id"]).subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.form = this._formBuilder.group({
                    company: [
                        {
                            value: "Focus",
                            disabled: true,
                        },
                        Validators.required,
                    ],
                    role: [res["role"], Validators.required],
                    description: [res["description"], Validators.required],
                });
            },
            (err) => console.log(err)
        );
    }
    onClick() {
        if (this.form.valid) {
            // alert("You Clicked Me!");
            var role = this.form.getRawValue();
            // { role: role["role"] }
            console.log(JSON.stringify(role));

            this.webReqservices
                .put("users/roleupdate/" + this.data["id"], {
                    role: role.role,
                    description: role.description,
                })
                .subscribe(
                    (data) => {
                        console.log(data);
                        // this.router.navigate(["/apps/images"]);
                        this.form.reset();
                        this.error = "Role successfully created";
                        this.dialogRef.close({
                            res: "success",
                            role: role.role,
                            description: role.description,
                        });
                    },
                    (error) => {
                        console.log(error);
                        this.error = error;
                        // this.loading = false;
                        this.dialogRef.close({ res: "err" });
                    }
                );
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
}
