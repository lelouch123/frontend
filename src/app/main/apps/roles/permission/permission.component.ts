import { Component, OnInit } from "@angular/core";
import { RoleService } from "app/_services/role.service";
import { WebRequestService } from "app/_services/web-request.service";
import { MatSnackBar } from "@angular/material";

@Component({
    selector: "app-permission",
    templateUrl: "./permission.component.html",
    styleUrls: ["./permission.component.scss"],
})
export class PermissionComponent implements OnInit {
    v: any;
    permissions: any = [
        "report.send",
        "report.views",
        "chat.views",
        "role.add",
        "role.views",
        "permission.views",
        "project.views",
        "project.add",
        "project.director",
        "project.member",
        "scrum.template",
        "resource.views",
        "user.views",
        "log.views",
        "team.views",
        "kpi.views",
        "customer.views",
        "risk.views",
        "dashboard.generale",
        "dashboard.project",
    ];
    permission_role: any = [];

    roles: any = [];

    constructor(
        private rolservice: RoleService,
        private webReqservices: WebRequestService,
        private _snackBar: MatSnackBar
    ) {}

    ngOnInit() {
        this.rolservice.getroles().subscribe(
            (data) => {
                // this.router.navigate(["/apps/images"]);
                this.roles = data;
                console.log("this.roles");
                console.log(this.roles);

                for (let i = 0; i < this.roles.length; i++) {
                    var obj = {
                        role: "",
                        permission: [],
                        id: "",
                    };
                    obj.role = this.roles[i].role;
                    obj.id = this.roles[i].id;

                    let p = [];
                    for (let j = 0; j < this.permissions.length; j++) {
                        var o = {
                            permission: "",
                            checked: false,
                        };

                        o.permission = this.permissions[j];
                        if (this.roles[i].permissions.includes(o.permission))
                            o.checked = true;
                        p[j] = o;
                    }
                    obj.permission = p;
                    this.permission_role[i] = obj;
                }
                console.log(this.permission_role);
            },
            (error) => {
                console.log(error);

                // this.loading = false;
            }
        );
    }
    save(index) {
        console.log(index);
        console.log(this.permission_role[index].id);
        let permissions = [];
        for (
            let i = 0;
            i < this.permission_role[index].permission.length;
            i++
        ) {
            if (this.permission_role[index].permission[i].checked) {
                permissions.push(
                    this.permission_role[index].permission[i].permission
                );
            }
        }
        console.log(permissions);
        this.webReqservices
            .put("users/roles/" + this.permission_role[index].id, {
                permissions,
            })
            .subscribe(
                (res) => {
                    console.log(res);
                    this._snackBar.open("Updated successfully", "Update", {
                        duration: 2000,
                    });

                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "Permission",
                            entityid: this.permission_role[index].id,
                            type: "update",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                                window.location.reload();
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                },
                (err) => {
                    console.log(err);
                }
            );
    }
}
