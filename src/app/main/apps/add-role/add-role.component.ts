import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { RoleService } from "app/_services/role.service";
import { first } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";
import { WebRequestService } from "app/_services/web-request.service";
@Component({
    selector: "app-add-role",
    templateUrl: "./add-role.component.html",
    styleUrls: ["./add-role.component.scss"],
})
export class AddRoleComponent implements OnInit {
    form: FormGroup;
    error: string;
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private _snackBar: MatSnackBar,
        private webReqservices: WebRequestService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Reactive Form
        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            role: ["", Validators.required],
            description: ["", Validators.required],
        });
    }

    /**
     * On destroy
     */

    onClick() {
        if (this.form.valid) {
            // alert("You Clicked Me!");
            var role = this.form.getRawValue();
            // { role: role["role"] }
            console.log(JSON.stringify(role));

            this.rolservice.creatRole(role.role, role.description).subscribe(
                (data) => {
                    console.log(data);
                    // this.router.navigate(["/apps/images"]);
                    this.form.reset();
                    this.error = "Role successfully created";
                    this._snackBar.open("Role successfully created", "create", {
                        duration: 2000,
                    });
                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "Role",
                            entityid: data["id"],
                            type: "add",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                },
                (error) => {
                    console.log(error);
                    this.error = error;
                    // this.loading = false;
                }
            );
        }
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
