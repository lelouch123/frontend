import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UploadimageComponent } from './uploadimage.component';


const routes: Routes = [
  { path: '', component: UploadimageComponent }
];

@NgModule({
  declarations: [UploadimageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class UploadimageModule { }
