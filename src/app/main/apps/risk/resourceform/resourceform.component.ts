import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
import { Board } from "../../scrumboard/board.model";
import { Card } from "../../scrumboard/card.model";
@Component({
    selector: "app-resourceform",
    templateUrl: "./resourceform.component.html",
    styleUrls: ["./resourceform.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceformComponent implements OnInit {
    form: FormGroup;
    error: string;
    projects: any = [];
    constructor(
        public dialogRef: MatDialogRef<ResourceformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],

            description: ["", Validators.required],
            action: ["", Validators.required],
            projectid: ["", Validators.required],

            impact: [
                "",
                [
                    Validators.required,
                    Validators.pattern("^[0-9]*$"),
                    Validators.minLength(1),
                ],
            ],
            probability: [
                "",
                [
                    Validators.required,
                    Validators.pattern("^[0-9]*$"),
                    Validators.minLength(1),
                ],
            ],
        });
        this.webReqservices.get("project/all").subscribe(
            (res: any) => {
                console.log(this.data);
                console.log(res);
                this.projects = res;
            },
            (err) => console.log(err)
        );

        if (this.data["type"] != "create") {
            this.webReqservices
                .get("risk/detaill/" + this.data["id"])
                .subscribe(
                    (res) => {
                        console.log("res");
                        console.log(res);
                        this.form = this._formBuilder.group({
                            company: [
                                {
                                    value: "Focus",
                                    disabled: true,
                                },
                                Validators.required,
                            ],

                            description: [
                                res["description"],
                                Validators.required,
                            ],
                            action: [res["description"], Validators.required],
                            projectid: [res["projectid"], Validators.required],
                            impact: [
                                res["impact"],
                                [
                                    Validators.required,
                                    Validators.pattern("^[0-9]*$"),
                                    Validators.minLength(1),
                                ],
                            ],
                            probability: [
                                res["probability"],
                                [
                                    Validators.required,
                                    Validators.pattern("^[0-9]*$"),
                                    Validators.minLength(1),
                                ],
                            ],
                        });
                    },
                    (err) => console.log(err)
                );
        }
    }
    onClick() {
        if (this.data["type"] == "create") {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices.get("project/" + role.projectid).subscribe(
                    (data: any) => {
                        console.log("project");
                        console.log(data.templateId);

                        this.webReqservices
                            .get("project/board/" + data.templateId)
                            .subscribe(
                                (response: any) => {
                                    console.log("tempalte");
                                    console.log(response);
                                    let board: any = {};
                                    board = new Board([]);

                                    var cards = JSON.parse(response.board.obj);
                                    if (cards.cards) board.cards = cards.cards;
                                    board.id = response.board.id;
                                    board.lists = response.board.lists;
                                    board.settings = response.board.settings;
                                    board.labels = response.board.labels;
                                    board.members = [];
                                    board.projectid = response.board.projectId;
                                    response.members.forEach((member) => {
                                        var m = {
                                            id: member.id,
                                            name: member.name,
                                            avatar:
                                                "http://localhost:4000/" +
                                                member.image,
                                        };
                                        board.members.push(m);
                                    });
                                    board.name = response.name;

                                    // board.lists[0].id, new Card({name: newCardName})
                                    let newCard = new Card({
                                        name: role.action,
                                    });
                                    board.lists.map((list) => {
                                        if (list.id === board.lists[0].id) {
                                            return list.idCards.push(
                                                newCard.id
                                            );
                                        }
                                    });

                                    board.cards.push(newCard);
                                    board.obj = { cards: board.cards };
                                    this.webReqservices
                                        // .post("api/scrumboard-boards/" + this.board.id, this.board)
                                        .put("project/board/update", board)
                                        .subscribe(
                                            (response) => {
                                                this.webReqservices
                                                    .post("risk/create", {
                                                        description:
                                                            role.description,
                                                        action: role.action,
                                                        impact: role.impact,
                                                        probability:
                                                            role.probability,
                                                        projectid:
                                                            role.projectid,
                                                    })
                                                    .subscribe(
                                                        (data) => {
                                                            console.log(data);
                                                            // this.router.navigate(["/apps/images"]);
                                                            this.form.reset();
                                                            this.error =
                                                                "Risk successfully created";
                                                            this.dialogRef.close(
                                                                {
                                                                    res:
                                                                        "success",
                                                                    resource: data,
                                                                }
                                                            );
                                                        },
                                                        (error) => {
                                                            console.log(error);
                                                            this.error = error;
                                                            // this.loading = false;
                                                            this.dialogRef.close(
                                                                { res: "err" }
                                                            );
                                                        }
                                                    );
                                            },
                                            (err) => {
                                                console.log(err);
                                            }
                                        );
                                },
                                (err) => {
                                    console.log(err);
                                }
                            );
                    },
                    (err) => {
                        console.log(err);
                    }
                );
            }
        } else {
            if (this.form.valid) {
                // alert("You Clicked Me!");
                var role = this.form.getRawValue();
                // { role: role["role"] }
                console.log(JSON.stringify(role));

                this.webReqservices
                    .put("risk/edit/" + this.data["id"], {
                        description: role.description,
                        action: role.action,
                        impact: role.impact,
                        probability: role.probability,
                        projectid: role.projectid,
                    })
                    .subscribe(
                        (data) => {
                            console.log(data);
                            // this.router.navigate(["/apps/images"]);
                            this.form.reset();
                            this.error = "risk successfully created";
                            this.dialogRef.close({
                                res: "success",
                                name: role.name,
                                description: role.description,
                                number: role.number,
                            });
                        },
                        (error) => {
                            console.log(error);
                            this.error = error;
                            // this.loading = false;
                            this.dialogRef.close({ res: "err" });
                        }
                    );
            }
        }
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
}
