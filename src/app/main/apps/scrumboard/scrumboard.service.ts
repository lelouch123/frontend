import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot,
} from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";
import { Board } from "./board.model";
import { WebRequestService } from "app/_services/web-request.service";

@Injectable()
export class ScrumboardService implements Resolve<any> {
    boards: any[];
    routeParams: any;
    board: any;

    onBoardsChanged: BehaviorSubject<any>;
    onBoardChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private webReqservices: WebRequestService
    ) {
        // Set the defaults
        this.onBoardsChanged = new BehaviorSubject([]);
        this.onBoardChanged = new BehaviorSubject([]);
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {
            Promise.all([this.getBoards()]).then(() => {
                resolve();
            }, reject);
        });
    }

    /**
     * Get boards
     *
     * @returns {Promise<any>}
     */
    getBoards(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .get("api/scrumboard-boards")
                .subscribe((response: any) => {
                    this.boards = response;
                    this.onBoardsChanged.next(this.boards);
                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "Boards",
                            entityid: "all boards",
                            type: "read",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                    resolve(this.boards);
                }, reject);
        });
    }

    /**
     * Get board
     *
     * @param boardId
     * @returns {Promise<any>}
     */
    getBoard(boardId): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .get("http://localhost:4000/project/board/" + boardId)
                .subscribe((response: any) => {
                    console.log("my board");

                    console.log(response);
                    // this.board = response;
                    // this.board = {
                    //     name: "best board ever",
                    //     cards: [],
                    //     lebels: [],
                    //     lists: [],
                    //     members: [],
                    //     settings: {},
                    // };
                    this.board = new Board([]);

                    var cards = JSON.parse(response.board.obj);
                    if (cards.cards) this.board.cards = cards.cards;
                    this.board.id = response.board.id;
                    this.board.lists = response.board.lists;
                    this.board.settings = response.board.settings;
                    this.board.labels = response.board.labels;
                    this.board.members = [];
                    this.board.projectid = response.board.projectId;
                    response.members.forEach((member) => {
                        var m = {
                            id: member.id,
                            name: member.name,
                            avatar: "http://localhost:4000/" + member.image,
                        };
                        this.board.members.push(m);
                    });
                    this.board.name = response.name;
                    console.log("this.board");
                    console.log(this.board);
                    this.onBoardChanged.next(this.board);

                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "Board",
                            entityid: response.board.id,
                            type: "read",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                    resolve(this.board);
                }, reject);
        });
    }

    /**
     * Add card
     *
     * @param listId
     * @param newCard
     * @returns {Promise<any>}
     */
    addCard(listId, newCard): Promise<any> {
        this.board.lists.map((list) => {
            if (list.id === listId) {
                return list.idCards.push(newCard.id);
            }
        });

        this.board.cards.push(newCard);

        return this.updateBoard();
    }

    /**
     * Add list
     *
     * @param newList
     * @returns {Promise<any>}
     */
    addList(newList): Promise<any> {
        this.board.lists.push(newList);

        return this.updateBoard();
    }

    /**
     * Remove list
     *
     * @param listId
     * @returns {Promise<any>}
     */
    removeList(listId): Promise<any> {
        const list = this.board.lists.find((_list) => {
            return _list.id === listId;
        });

        for (const cardId of list.idCards) {
            this.removeCard(cardId);
        }

        const index = this.board.lists.indexOf(list);

        this.board.lists.splice(index, 1);

        return this.updateBoard();
    }

    /**
     * Remove card
     *
     * @param cardId
     * @param listId
     */
    removeCard(cardId, listId?): void {
        const card = this.board.cards.find((_card) => {
            return _card.id === cardId;
        });

        if (listId) {
            const list = this.board.lists.find((_list) => {
                return listId === _list.id;
            });
            list.idCards.splice(list.idCards.indexOf(cardId), 1);
        }

        this.board.cards.splice(this.board.cards.indexOf(card), 1);

        this.updateBoard();
    }

    /**
     * Update board
     *
     * @returns {Promise<any>}
     */
    updateBoard(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.board.obj = { cards: this.board.cards };
            this._httpClient
                .post("api/scrumboard-boards/" + this.board.id, this.board)
                // .put("http://localhost:4000/project/board/update", this.board)
                .subscribe((response) => {
                    // console.log("//////////");
                    // console.log(response);
                    // console.log("//////////");
                    this.onBoardChanged.next(this.board);

                    resolve(this.board);
                }, reject);
        });
    }
    updateBoard1(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.board.obj = { cards: this.board.cards };
            this._httpClient
                // .post("api/scrumboard-boards/" + this.board.id, this.board)
                .put("http://localhost:4000/project/board/update", this.board)
                .subscribe((response) => {
                    console.log("//////////");
                    console.log(response);
                    console.log("//////////");
                    this.onBoardChanged.next(this.board);
                    resolve(this.board);
                    /* Shadowing operation */
                    this.webReqservices
                        .post("shadow/", {
                            entity: "Board",
                            entityid: this.board.id,
                            type: "update",
                            user: JSON.parse(
                                localStorage.getItem("currentUser")
                            ).name,
                        })
                        .subscribe(
                            (res) => {
                                console.log("shadowing");
                                console.log(res);
                            },
                            (err) => console.log(err)
                        );
                    /* End of shadowing operation */
                }, reject);
        });
    }
    /**
     * Update card
     *
     * @param newCard
     */
    updateCard(newCard): void {
        this.board.cards.map((_card) => {
            if (_card.id === newCard.id) {
                return newCard;
            }
        });

        this.updateBoard();
    }

    /**
     * Create new board
     *
     * @param board
     * @returns {Promise<any>}
     */
    createNewBoard(board): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .post("api/scrumboard-boards/" + board.id, board)
                .subscribe((response) => {
                    resolve(board);
                }, reject);
        });
    }
}

@Injectable()
export class BoardResolve implements Resolve<any> {
    /**
     * Constructor
     *
     * @param {ScrumboardService} _scrumboardService
     */
    constructor(private _scrumboardService: ScrumboardService) {}

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @returns {Promise<any>}
     */
    resolve(route: ActivatedRouteSnapshot): Promise<any> {
        return this._scrumboardService.getBoard(route.paramMap.get("boardId"));
    }
}
