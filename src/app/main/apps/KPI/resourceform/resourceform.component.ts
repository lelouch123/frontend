import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Inject,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormArray,
    FormControl,
} from "@angular/forms";
import { RoleService } from "app/_services/role.service";
import { DialogData } from "assets/angular-material-examples/dialog-data/dialog-data-example";
import { WebRequestService } from "app/_services/web-request.service";
@Component({
    selector: "app-resourceform",
    templateUrl: "./resourceform.component.html",
    styleUrls: ["./resourceform.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceformComponent implements OnInit {
    form: FormGroup;
    error: string;
    projects: any = [];
    forumla: Number;
    constructor(
        public dialogRef: MatDialogRef<ResourceformComponent>,
        private _formBuilder: FormBuilder,
        private rolservice: RoleService,
        private webReqservices: WebRequestService,

        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit() {
        console.log(this.data);

        this.form = this._formBuilder.group({
            company: [
                {
                    value: "Focus",
                    disabled: true,
                },
                Validators.required,
            ],
            kpivalue: [
                {
                    value: "Not calculated yet",
                    disabled: true,
                },
                Validators.required,
            ],
            name: ["", Validators.required],
            projected: ["", Validators.required],
            target: new FormControl("", [
                Validators.required,
                Validators.pattern("^[0-9]*$"),
                Validators.minLength(1),
            ]),
            description: ["", Validators.required],
            fields: new FormArray([]),
            formula: ["", Validators.required],
        });
        this.webReqservices.get("project/all").subscribe(
            (res) => {
                console.log("res");
                console.log(res);
                this.projects = res;

                // console.log(this.projects);
            },
            (err) => {
                console.log(err);
            }
        );
        if (this.data["type"] != "create") {
            this.webReqservices.get("kpi/detaill/" + this.data["id"]).subscribe(
                (res) => {
                    console.log("res");
                    console.log(res);
                    this.form = this._formBuilder.group({
                        company: [
                            {
                                value: "Focus",
                                disabled: true,
                            },
                            Validators.required,
                        ],
                        kpivalue: [
                            {
                                value: res["kpivalue"],
                                disabled: true,
                            },
                            Validators.required,
                        ],
                        name: [res["name"], Validators.required],
                        projected: [res["projected"], Validators.required],
                        target: new FormControl(res["target"], [
                            Validators.required,
                            Validators.pattern("^[0-9]*$"),
                            Validators.minLength(1),
                        ]),
                        description: [res["description"], Validators.required],
                        fields: new FormArray([]),
                        formula: [res["formula"], Validators.required],
                    });
                    const control = <FormArray>this.form.get("fields");

                    res["fields"].forEach((element) => {
                        console.log("element");
                        console.log(element);
                        control.push(
                            new FormGroup({
                                name: new FormControl(element.name),
                                value: new FormControl(element.value, [
                                    Validators.required,
                                    Validators.pattern("^[0-9]*$"),
                                    Validators.minLength(1),
                                ]),
                            })
                        );
                    });
                },
                (err) => console.log(err)
            );
        }
    }
    onClick() {
        if (this.data["type"] == "create") {
            if (this.form.valid) {
                var role = this.form.getRawValue();
                var v = this.calculate(role.formula);
                this.form.patchValue({
                    kpivalue: this.forumla,
                });
                role = this.form.getRawValue();
                console.log("role");
                console.log(role);

                var v = this.calculate(role.formula);
                if (v) {
                    this.webReqservices
                        .post("kpi/create", {
                            target: role.target,
                            description: role.description,
                            kpivalue: role.kpivalue,
                            name: role.name,
                            projected: role.projected,
                            fields: role.fields,
                            formula: role.formula,
                        })
                        .subscribe(
                            (data) => {
                                console.log("data");

                                console.log(data);

                                this.form.reset();

                                this.error = "Kpi successfully created";

                                this.dialogRef.close({
                                    res: "success",
                                    resource: data,
                                });
                            },
                            (error) => {
                                console.log("error");

                                console.log(error);
                                this.error = error;

                                this.dialogRef.close({ res: "err" });
                            }
                        );
                } else {
                    console.log("formula not valid");
                    this.form.patchValue({
                        formula: "",
                    });
                }
            }
        } else {
            if (this.form.valid) {
                var role = this.form.getRawValue();
                var v = this.calculate(role.formula);
                this.form.patchValue({
                    kpivalue: this.forumla,
                });
                role = this.form.getRawValue();
                console.log("role");
                console.log(role);

                if (v) {
                    console.log("v");
                    console.log(v);

                    this.webReqservices
                        .put("kpi/edit/" + this.data["id"], {
                            target: role.target,
                            description: role.description,
                            kpivalue: role.kpivalue,
                            name: role.name,
                            projected: role.projected,
                            fields: role.fields,
                            formula: role.formula,
                        })
                        .subscribe(
                            (data) => {
                                console.log("data");

                                console.log(data);

                                this.form.reset();

                                this.error = "Kpi successfully created";
                                console.log({
                                    target: role.target,
                                    description: role.description,
                                    kpivalue: role.kpivalue,
                                    name: role.name,
                                    projected: role.projected,
                                    fields: role.fields,
                                    formula: role.formula,
                                });
                                this.dialogRef.close({
                                    res: "success",
                                    resource: {
                                        target: role.target,
                                        description: role.description,
                                        kpivalue: role.kpivalue,
                                        name: role.name,
                                        projected: role.projected,
                                        fields: role.fields,
                                        formula: role.formula,
                                    },
                                });
                            },
                            (error) => {
                                console.log("error");

                                console.log(error);
                                this.error = error;

                                this.dialogRef.close({ res: "err" });
                            }
                        );
                } else {
                    console.log("formula not valid");
                    this.form.patchValue({
                        formula: "",
                    });
                }
            }
        }
    }
    calculate(s) {
        /**calculate the formula */
        var tab = s.split(" ");
        var role = this.form.getRawValue();

        let ch = "";
        for (var i = 0; i < s.length; i++) {
            if (s[i].toLowerCase() == "f") {
                if (
                    role.fields.length < parseInt(s[i + 1]) ||
                    !parseInt(s[i + 1])
                ) {
                    return false;
                }

                ch = ch + role.fields[s[i + 1] - 1].value;
                i++;
            } else {
                ch = ch + s[i];
            }
        }

        try {
            eval(ch);
            this.forumla = eval(ch);
            console.log(this.forumla);
            return true;
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.log("error");
                return false;
            }
        }
        return false;
        /**end of calculating the formula */
    }
    close() {
        this.dialogRef.close({ res: "cancel" });
    }
    initfields() {
        return new FormGroup({
            name: new FormControl(""),
            value: new FormControl("", [
                Validators.required,
                Validators.pattern("^[0-9]*$"),
                Validators.minLength(1),
            ]),
        });
    }
    addfields() {
        const control = <FormArray>this.form.get("fields");
        control.push(this.initfields());
        var role = this.form.getRawValue();
        // { role: role["role"] }
        console.log(role);
    }
    removefields(i) {
        const control = <FormArray>this.form.get("fields");
        control.removeAt(i);
    }

    getfields(form) {
        //console.log(form.get('sections').controls);
        return form.controls.fields.controls;
    }
}
